/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "alleffects.hpp"

std::string returnEffectEngineName(int i)  //to add a new effect please add its name (last position so it will be the else)
{
	if(i==0)
		return "Phaser";
	else if(i==1)
		return "Disto";
	else if (i==2)
		return "FIR Filter";
	else 
		return "IIR Filter";
	
}

Effect* initEffect(int eng)	//to add a new effect please add its contructor (last position so it will be the else)
{
	Effect* temp;
	if(eng==0)
		temp=new Phaser();
	else if(eng==1)
		temp=new Disto();
	else if(eng==2)
		temp=new FIRFilter();
	else
		temp=new IIRFilter();
	return temp;
}
