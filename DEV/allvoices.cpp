/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "allvoices.hpp"


std::string returnEngineName(int eng) //to add an engine please add its name here, before the last else (the last engine must be "Void")
{
	std::string temp;
	if(eng==0)
		temp="Karplus-Strong";
	else if(eng==1)
		temp="Additive";
	else
		temp="Void";
	return temp;
}

Voice* initEngine(int i,std::mutex* mutexVar,int eng)//to add an engine please add its constructor here, before the last else (the last engine must be "Void")
{
	Voice* temp;
	if(eng==0)
		temp=new KarplusStrong(i,BUFF,mutexVar);
	else if(eng==1)
		temp= new Additive(i,BUFF,mutexVar);
	else
		temp=new VoidVoice(i,BUFF,mutexVar);
	return temp;
}
