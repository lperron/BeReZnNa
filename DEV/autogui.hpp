/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _AUTOGUI_HPP_
#define _AUTOGUI_HPP_
/*
	AutoGui is... the GUI
	If autogui do what it is designed to do you won't even have to look how it works
	because it works
	Autogui will generate a basic gui for all effects and engines allowing a direct access to parameters
	
*/
#include "main.hpp"
//Well... too many includes
#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gtkmm/notebook.h>
#include <gtkmm/stock.h>
#include <gtkmm/window.h>
#include <gtkmm/scale.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/textview.h>
#include <gtkmm/listbox.h>
#include <gtkmm/alignment.h>
#include <gtkmm/switch.h>
#include <gtkmm/frame.h>
#include <gtkmm/levelbar.h>
#include <gtkmm/spinbutton.h>
#include <glibmm/main.h> //for timer
#include <gtkmm/comboboxtext.h>
#include <gtkmm/image.h>
#include <gtkmm/toolbutton.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/flowbox.h>
#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/dialog.h>
#include <gtkmm/filefilter.h>
#include <gtkmm/revealer.h>
#include <gtkmm/image.h>
#include <gtkmm/entry.h>
#include "voicesmanager.hpp"
#include "mixer.hpp"
#include "midi.hpp"

/*class Page : public Gtk::VBox
{
	public
}*/

//the custom signals we're gonna use
typedef sigc::signal<void, int,int,int,int> signalNewEngine;
typedef sigc::signal <void,int,int> signalEffectChange;
typedef sigc::signal <void,int> signalUpMix;


class PresetDial : public Gtk::Dialog
{
	public :
	PresetDial(Gtk::Window* parent);
	std::string get_text();
	private :
	Gtk::Box* mainBox;
	Gtk::Label labelText;
	Gtk::Entry textEntry;
};
class VoicePage : public Gtk::VBox	//same as effect but for engine //I'm not gonna comment this part
{
	public :
	VoicePage(VoicesManager* AMannix,int group, Midi* AMidas,Gtk::Window* par);
	void slide(int scale, int param);
	void listChange(Gtk::ListBoxRow* row,int param);
	void refresh();
	void save();
	void load();
	~VoicePage();
	private :
	Gtk::Window* parent;
	Midi* Midas;
	VoicesManager* Mannix;
	int nbParam;
	int nbScales;
	int nbLists;
	param* VoiceParams;
	int voiceIndex;
	Gtk::Label* Name;
	Gtk::Alignment* voidAl;
	Gtk::Alignment* NameAl;
	Gtk::Alignment* GroupAl;
	Gtk::Label* GroupIndex;
	Gtk::Label* ParamName;
	Gtk::HBox ScaleBox;
	Gtk::HBox LabelBox;
	Gtk::VBox* ParamBox;
	//Gtk::Button* testButton;
	Gtk::Scale*** paramScale;	//Wellllll I don't think we need a triple pointer for this, but it's working
	Gtk::ListBox* paramList;
	Gtk::Label** paramListName;
	//preset Box
	Gtk::HBox presetBox;
	Gtk::ComboBoxText presetCombo;
	Gtk::Button savePreset;
 	//Glib::RefPtr<Gtk::Adjustment> scaleAdjustment;
	
};

class EffectPage : public Gtk::VBox	
{
	public :
	EffectPage(Mixer* Yann,int i,Midi* AMidas,Gtk::Window* par);
	void slide(int scale,int param);	//when the value of a slider changes
	void listChange(Gtk::ListBoxRow*,int param);	//when the selected item of a list changes
	void onOff(Gtk::StateFlags dontCare);	//to activate/deactivate an effect
	void refresh();
	void save();
	void load();
	~EffectPage();
	private :
	Gtk::Window* parent;
	Mixer* Moix;
	Midi* Midas;
	int effectIndex;	//which effect am I
	bool active;	//am I on
	int nbParam;	//how many param (lists+scales) do I have
	int nbScales;	//how many scales(slider)
	int nbLists;	//lists
	param* EffectParams;	//the param struc of the effect (see main.hpp for detail)

	//Info
	Gtk::Label* Name;	//my name
	Gtk::Alignment* NameAl;	//just to put things at the right place
	Gtk::Alignment* EffectAl;	//same
	Gtk::Alignment* SwitchAl;	//same
	Gtk::Label* EffectIndex;	// to write the index
	Gtk::Switch OnOff;	
	Gtk::Label* ParamName;	// names of each parameters
	
	//Parameters box
	Gtk::HBox ScaleBox;	//the container for the parameters's controls
	Gtk::HBox LabelBox;	//for the name
	Gtk::VBox* ParamBox;	//param box = 1param (scale or list) + its name 
	Gtk::Scale*** paramScale;	//all the sliders
	Gtk::ListBox* paramList;	//all the lists
	Gtk::Label** paramListName;	//all the names
	//preset Box
	Gtk::HBox presetBox;
	Gtk::ComboBoxText presetCombo;
	Gtk::Button savePreset;
};

class PopUpAdd :public  Gtk::Window	//pop up to add a new group
{
	public :
	PopUpAdd(VoicesManager* AMannix,signalNewEngine* signal);
	void go(); //when the pop up is activated
	void on_cancel_clicked();//when cancel button is clicked
	void on_ok_clicked();	//ok
	void unGo(){hide();};	//when the pop up is ended
	void on_spin_change();  //when the user changes the note range of the group

	~PopUpAdd();
	private :

	signalNewEngine* signalAdd;	//send this signal if an engine is added, usefull for Gui 
	Gtk::VBox MainBox;	//main container
	VoicesManager* tempMannix;
	//std::string* enginesName; 

	//Engine Box, where we choose the engine and the number of voices assigned to it
	int freeVoices;
	Gtk::HBox EngineBox;
	Gtk::VBox SubBox[2];
	Gtk::Label LabelEng[2];
	Gtk::ComboBoxText ListEngine;
	Gtk::SpinButton SpinVoice;
	//Gtk::Label* ListLabel;
	//and spin

	//bottom box	//where ok and cancel buttons are
	Gtk::HBox BottomBox;
	Gtk::Button OkButton;
	Gtk::Button CancelButton;
	//Spin Box	//where we select the note range
	Gtk::HBox SpinBox;
	Gtk::VBox LSBox[2];
	Gtk::Label LabelSpin[2];
	Gtk::SpinButton Spin[2];	//the first spin is for the lowest note and the second for the highest
};

class GlobalPage: public Gtk::VBox	//global page, with a group mixer, effect selection and main volume
{	
	public :
	GlobalPage(VoicesManager* AMannix,Mixer* AMoix,signalNewEngine* signalAdd,signalEffectChange* signalC,signalUpMix* signalU, Midi* AMidas);
	void on_add_clicked();	//to open the pop up window 
	void on_eff_change(int eff);	//when the user wants to change one effect
	void slide();	//to change main volume
	void mix_slide(int scale);	//mixer sliders, what to do to change volume of one group
	bool on_filter(Gtk::FlowBoxChild* child); //flowbox filter , all the group sliders exist but we show only the ones wich correspond to an active group
	void onUp(int gro);// when a group is added or deleted
	void onMute(int i);//to mute a group
	void onSolo(int i);//to solo a group
	void refresh();
	~GlobalPage();
	private :
	bool* mutedbysolo;
	Midi* Midas;
	//bool soloOn;
	signalEffectChange* signalChange;	//see  class Gui
	signalUpMix* signalUp;	//signal emitted by gui when the user adds or deletes a group
	GroupData* GroupInfo;	//group info from voices manager (for detail, see main.hpp)
	int nbEffects;
	signalNewEngine* signalAdd;	//signal emitted by pop up add when the user wants a new group
	PopUpAdd* AddWin;	//our pop up add, showed if the user clicks on add engine
	VoicesManager* Man;	
	Mixer* Mi;
	Gtk::HBox MainBox;	//container to all things
	//top label box
	Gtk::HBox TopLabelBox;	

	//Voices
	Gtk::Button AddV;	//"add engine"
	
	//mixer flowbox
	Gtk::FlowBox VoicesBox;
	Gtk::VBox* MixerChannel;
	Gtk::Label* ChannelLabel;
	Gtk::Scale** ChannelVolume;
	Gtk::ToggleButton* ChannelMute;
	Gtk::ToggleButton* ChannelSolo;



	//Eff
	Gtk::VBox EffectBox;
	Gtk::ComboBoxText* EffectComb;
	Gtk::Label* ComboLabel;
	Gtk::VBox* SubBoxEff;
	
	//vol
	Gtk::VBox VolBox;
	Gtk::Label VolumeLabel;
	Gtk::Scale VolumeScale;
	
	



	

};
class TuningSettings : public Gtk::Dialog
{
	public :
		TuningSettings(Gtk::Window* parent,int key,float freq);
		int getKey();
		float getFreq();
	private :
		Gtk::Box* mainBox;
		Gtk::HBox SpinBox;
		Gtk::VBox LSBox[2];
		Gtk::Label LabelSpin[2];
		Gtk::SpinButton Spin[2];
};

class DeviceSettings : public Gtk::Dialog
{
	public :
		DeviceSettings(Gtk::Window* parent,Midi* DMidas);
		int getId();
		//~DeviceSettings(){delete[] ComboLabel;};
	private :
		int nbDevices;
		midiDevice* devices;
		Gtk::Box* dialog;
		Gtk::HBox mainBox;
		Gtk::VBox midiBox;
		
		Gtk::Label ComboName;
		Gtk::ComboBoxText DevicesList;
		//Gtk::Label* ComboLabel;
};

class ChannelSelection : public Gtk::Dialog
{
	public :
	ChannelSelection(Gtk::Window* parent,Midi* DMidas);
	int getChannel(){return spin.get_value()-1;};
	private :
	Gtk::Box* dialog;
	Gtk::HBox mainBox;
	Gtk::VBox spinBox;
	
	Gtk::Label spinLabel;
	Gtk::SpinButton spin;
};

class Gui : public Gtk::Window //the main window
{
	public :
	Gui(VoicesManager* AMannix,Mixer* AMoix,Midi* AMidas, std::string Awhereami);
	bool onKeyPress(GdkEventKey* event);	//for virtual keyboard, note on
	bool onKeyRelease(GdkEventKey* event);	//for virtual keyboard, note off
	void addVoice(int nbvoice,int eng,int min,int max); //to add a new egine's group
	void changeEffect(int eff, int eng);
	void on_delete_page_clicked(int gro);	//to remove an engine
	void tuning();
	void tuningSettings();
	void deviceSelection();
	void channelSel();
	void toggleMapping();
	void toggleUnmapping();
	bool refreshValue();
	void unmapAllClicked();
	bool abracadabra();
	~Gui();
	private :
	std::string whereami;
	bool pressed[12]; //virtual keyboard, we need to keep track of what is pressed or not
	int nbVoicePages;//the number of engine's group
	int nbEffects;//number of effects
	signalNewEngine signalAdd;	//signal emitted by the pop up add page when the user wants a new group
	signalEffectChange signalChange;//signal emitted by global page when the user wants to change one effect
	signalUpMix signalUp;//signal emitted by Gui when a page is added or deleted
	VoicesManager* tempMannix;
	Mixer* tempMoix;
	Midi* Midas;
	midiDevice* devices;
	VoicePage** Voice;	//dynamic array of voice pages for the notebook
	EffectPage** Effect;	//same with effects
	GlobalPage* Glob;	//global page for notebook
Gtk::IconSize small;	//i don't think I use this //but i'm not sure anymore...
	Gtk::Notebook Bookie;	//our notebook
	Gtk::VBox MainBoxV;	//container for all the other things
	Gtk::VBox Global;	//same

	//tab Label
	Gtk::HBox* PageTab;	//our tab
	Gtk::Label* PageLabel;	//the name of the tab
	Gtk::ToolButton* DeletePage;	//the small cross button to delete a page
	Gtk::VBox* DeleteBox;	//container for the button

	//foot Label box
	Gtk::HBox FootLabelBox;	//foot info bar
	Gtk::Label info[5];	//all informations we want to be in the foot info bar
	Gtk::Label MappingLabel;
	//Menu
	Gtk::Revealer tada;
	Gtk::Image record;
	sigc::connection Merlin;
	Gtk::MenuBar Menu;
		Gtk::MenuItem TuningEdit;
			Gtk::Menu TuningMenu;
				Gtk::MenuItem Tuning;
				Gtk::MenuItem refTuning;
		Gtk::MenuItem Midim;
		Gtk::HBox MidiBox;
		Gtk::Label MidiLabel;
			Gtk::Menu MidiMenu;
				Gtk::MenuItem selDevice;
				Gtk::MenuItem chanSel;
				Gtk::MenuItem mappingOn;
				Gtk::MenuItem unmappingOn;
				Gtk::MenuItem unmapAll;



};


#endif
