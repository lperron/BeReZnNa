/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "disto.hpp"


Disto::Disto():Effect("Disto",4)//name,number of params
{

	params[0].name="Dry/Wet";	//we set all params
	params[0].value=1;
	params[0].list=0;
	params[0].listNames=NULL;
	params[0].midiValue=127;
	params[1].name="Offset";
	params[1].value=0;
	params[1].list=0;
	params[1].listNames=NULL;
	params[1].midiValue=(0+1*127)/2;
	params[2].name="Clipping Mode";
	params[2].value=1;
	params[2].midiValue=1;
	params[2].list=3;	//a list with 3 elements
	params[2].listNames=new string[3];//names 
	params[2].listNames[0]="Hard";
	params[2].listNames[1]="Soft";
	params[2].listNames[2]="Fold";
	params[3].name="Drive";
	params[3].value=10;
	params[3].list=0;
	params[3].listNames=NULL;
	params[3].midiValue=10/(float)(2);

}

void Disto::run(float* buffy,int bufferSize)	//what to do when mixer give us some data to process
{
	
	if(params[2].value==1)//soft clipping, cubic
	{
		float temp=2/3.;
		float tempb;
		for(int i=0;i<bufferSize;i++)
		{
			tempb=buffy[i]*params[3].value+params[1].value;
			if(tempb<-1)
				buffy[i]=(1-params[0].value)*buffy[i]-params[0].value*temp;
			else if(tempb>1)
				buffy[i]=(1-params[0].value)*buffy[i]+params[0].value*temp;
			else
				buffy[i]=(1-params[0].value)*buffy[i]+params[0].value*(tempb-tempb*tempb*tempb/3.);
			
		}
	}
	else if(params[2].value==0) //hard Clipping
	{
		float tempb;
		for(int i=0;i<bufferSize;i++)
		{
			tempb=buffy[i]*params[3].value+params[1].value;
			if(tempb<-1)
				buffy[i]=(1-params[0].value)*buffy[i]-params[0].value*1;
			else if(tempb>1)
				buffy[i]=(1-params[0].value)*buffy[i]+params[0].value*1;
			else
				buffy[i]=(1-params[0].value)*buffy[i]+params[0].value*tempb;
		}
	}
	else if(params[2].value==2) //Fold
	{
		float tempb;
		for(int i=0;i<bufferSize;i++)
		{
			tempb=buffy[i]*params[3].value+params[1].value;
			do
			{
				if(tempb>1)
					tempb=2-tempb;
				if(tempb<-1)
					tempb=-2-tempb;
			}while(tempb>1 or tempb<-1);
			buffy[i]=(1-params[0].value)*buffy[i]+params[0].value*tempb;
		}
	}

}


void Disto::modifyParameter(int param,int value)//legacy, don't care
{
	;
}
void Disto::setParameter(int param,int value)//keep in mind that the value is a 8bit int
						//so you will need to scale it to your parameters amplitude, you can do whatever you want with this but don't forget the params[i].midiValue=value;
{
	if(param==0)
	{
		params[0].value=value/127.;//between 0 & 1
		params[0].midiValue=value;
	}
	else if(param==1)
	{
		float temp=value*2/127.-1;//between -1 & 1
		params[1].value=temp;
		params[1].midiValue=value;
	}
	else if(param==2)
	{
		int temp=value;
		if(value>=0 && value<3)	//Well normally Autogui won't send any wrong value, but safetly first
		{
			params[2].value=temp;
			params[2].midiValue=value;
		}
	}
	else if(param==3)
	{
		params[3].value=value*2; //between 0 and 254
		params[3].midiValue=value;
		
	}
}

Disto::~Disto()
{
	;
}
