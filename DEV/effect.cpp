/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "effect.hpp"

using namespace std;

Effect::Effect(string Ename ,int nbparam)	//basic constructor, the params dynamic array is allocated here, but not the names list of a list please refer to disto.cpp or phaser.cpp or IIR or FIR
{
	name=Ename;
	active=false;
	numberParams=nbparam;
	params=new param[nbparam];
	
}
void Effect::loadPreset(int preset)
{
	if(preset<128)
	{
		string bankName="./Presets/"+name+".blob";
		string line;
		ifstream fpreset(bankName.c_str(),ios::in);
		if(fpreset)
		{
			for(int i=0;i<=preset;i++)
				getline(fpreset,line);
			istringstream temp(line);
			string bin;
			temp>>bin;	//preset name,  params 0 .value, params 0.midiValue,...
			for(int i=0;i<numberParams;i++)
				temp>>params[i].value>>params[i].midiValue;
			fpreset.close();
		}
		else
		{
			createBank();
		}	

	}
}
string Effect::getPresetName(int preset)
{
	if(preset<128)
	{
		string bankName="./Presets/"+name+".blob";
		string line;
		string name;
		ifstream fpreset(bankName.c_str(),ios::in);
		if(fpreset)
		{
			for(int i=0;i<=preset;i++)
				getline(fpreset,line);
			istringstream temp(line);
			string bin;
			temp>>name;	//preset name,  params 0 .value, params 0.midiValue,...
			for(int i=0;i<numberParams;i++)
				temp>>bin>>bin;
			fpreset.close();
			return name;
		}
		else
		{
			createBank();
			return "default";
			
		}	

	}
	return "None";
}

void Effect::savePreset(int preset,string presetName)
{
		//int i=0;
		string bankName="./Presets/"+name+".blob";
		string line;
		string tempBank;
		ostringstream stempBank;
		ifstream fpreseta(bankName.c_str(),ios::in);
		if(fpreseta)
		{
			for(int i=0;i<128;i++)
			{
				getline(fpreseta,line);
				if(preset!=i)
					stempBank<<line<<endl;
				else
				{
					stempBank<<presetName<<" ";
					for(int j=0;j<numberParams;j++)
						stempBank<<params[j].value<<" "<<params[j].midiValue<<" ";
					stempBank<<endl;
				}
			}
			fpreseta.close();
		}
		tempBank=stempBank.str();
		ofstream fpreset(bankName.c_str(),ios::out | ios::trunc);
		if(fpreset)
		{
			fpreset<<tempBank;
			fpreset.close();
		}
}


void Effect::createBank()
{
		string bankName="./Presets/"+name+".blob";
		string presetName="Default";
		ofstream fpreset(bankName.c_str(),ios::out | ios::trunc);
		if(fpreset)
		{
			for(int i=0;i<128;i++)
			{
				fpreset<<presetName<<" ";
				for(int j=0;j<numberParams;j++)
					fpreset<<params[j].value<<" "<<params[j].midiValue<<" ";
				fpreset<<endl;
			}
			fpreset.close();
		}
}

Effect::~Effect()	//destructor
{
	for(int i=0;i<numberParams;i++)
	{
		if(params[i].list!=0)
			delete[] params[i].listNames;
	}
	delete[] params;
}
