/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "firfilter.hpp"

using namespace std;




FIRFilter::FIRFilter():Effect("FIR Filter",5)//name, nb params
{
	//window jump, just for the fun
	//all these windows are in utils.cpp
	windowJump[0]=Wrectangular;
	windowJump[1]=Wbartlett;
	windowJump[2]=Whamming;
	windowJump[3]=Whanning;
	windowJump[4]=Wblackman;
	windowJump[5]=Wrand;

	//params init
	params[0].name="Dry/Wet";
	params[0].value=1;
	params[0].list=0;//not a list
	params[0].listNames=NULL;//so no allocation
	params[0].midiValue=127;
	params[1].name="F";
	params[1].value=2000;
	params[1].list=0;
	params[1].listNames=NULL;
	params[1].midiValue=2000/10000.*127;
	params[2].name="Order";
	params[2].value=42;
	params[2].list=0;
	params[2].listNames=NULL;
	params[2].midiValue=21;
	params[3].name="Window";
	params[3].value=1;
	params[3].midiValue=1;
	params[3].list=6;//list with 6 items
	params[3].listNames=new string[6];//so we need an allocation
	params[3].listNames[0]="Rectangular";//and we name the different items
	params[3].listNames[1]="Bartlett";
	params[3].listNames[2]="Hamming";
	params[3].listNames[3]="Hanning";
	params[3].listNames[4]="Blackman";
	params[3].listNames[5]="Random";
	params[4].name="Fear";
	params[4].value=0;
	params[4].midiValue=0;
	params[4].list=0;
	params[4].listNames=NULL;
	cutoff=params[1].value/(float)SAMPLE_RATE; //normalized freq //SAMPLE_RATE and other usefull constants are defined in main.hpp
	//now go to wikipedia and read the article about FIR filter
	length=params[2].value+1;
	lbuffer=new float[length];//well, stereo
	rbuffer=new float[length];
	for(int i=0;i<length;i++)
	{
		lbuffer[i]=rbuffer[i]=0;
	}	
	sincWeights=new float[length];
	weights=new float[length];
	windowWeights=windowJump[(int)params[3].value](params[2].value);
	computeWeights();
	windowing();
}

void FIRFilter::computeWeights()//just sinc
{
	float temp=params[2].value/2.;

	for(int i=0;i<length;i++)
	{
		if(i!=temp)
		{
			float tempb=PI*(i-temp);
			sincWeights[i]=sin(2*cutoff*tempb)/tempb;
		}	
		else
			sincWeights[i]=2*cutoff;
	}

}

void FIRFilter::windowing()//sinc * a window
{
	for(int i=0;i<length;i++)
	{
		weights[i]=windowWeights[i]*sincWeights[i];
	}
}

void FIRFilter::run(float* input,int inputSize)
{
	float templ=0;
	float tempr=0;
	for(int i=0;i<inputSize/2;i++)	//stereo, buffer =lrlrlr...lr
	{
		templ*=params[4].value;
		tempr*=params[4].value;

			if(index>=length)
				index=0;
			lbuffer[index]=input[2*i];
			rbuffer[index]=input[2*i+1];
			for(int i=0;i<length;i++)
			{
				int tempindex=index-i;
				if(tempindex<0)
					tempindex+=length;
				templ+=lbuffer[tempindex]*weights[i];
				tempr+=rbuffer[tempindex]*weights[i];
				
			}
			input[2*i]*=(1-params[0].value);
			input[2*i]+=params[0].value*templ;
			input[2*i+1]*=(1-params[0].value);
			input[2*i+1]+=params[0].value*tempr;
			index++;
			
	}
}

void FIRFilter::modifyParameter(int param,int value)
{
	;
}

void FIRFilter::setParameter(int param,int value)
{
	if(param==0)
	{
		params[0].value=value/127.;//between 0 and 1
		params[0].midiValue=value;
	}
	else if(param==1)
	{
		params[1].value=value/127.*10000; //between 0 and 10000
		params[1].midiValue=value;
		cutoff=params[1].value/(float)SAMPLE_RATE; //normalized freq
							//we don't use the freq in Hz
		computeWeights();
		windowing();
	}
	else if(param==2)
	{
		int temp=value*2;
		if(value<2)
			temp=2;
		params[2].value=temp;
		params[2].midiValue=(int)value;
		
		length=params[2].value+1;
		delete[] lbuffer;
		lbuffer=new float[length];//well, stereo
		delete[] rbuffer;
		rbuffer=new float[length];
		for(int i=0;i<length;i++)
		{
			lbuffer[i]=rbuffer[i]=0;
		}	
		delete[] sincWeights;
		sincWeights=new float[length];
		delete[] weights;
		weights=new float[length];
		windowWeights=windowJump[(int)params[3].value](params[2].value);
		computeWeights();
		windowing();
	}
	else if(param==3)
	{
		params[3].value=value;
		params[3].midiValue=value;
		windowWeights=windowJump[(int)params[3].value](params[2].value);
		computeWeights();
		windowing();
	}
	else if(param==4)
	{
		params[4].value=value/(float)127;
		params[4].midiValue=value;
		
	}

}


FIRFilter::~FIRFilter()//delete all new things except namelist wich is destroyed by ~Effect() 
{
	delete[] lbuffer;
	delete[] rbuffer;
	delete[] sincWeights;
	delete[] windowWeights;
	delete[] weights;
}
