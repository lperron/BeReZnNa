/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "iirfilter.hpp"

using namespace std;
IIRFilter::IIRFilter() :Effect("IIR Filter",6)//name, number of params
{
	params[0].name="Dry/Wet";
	params[0].value=1;
	params[0].list=0;//not a list
	params[0].listNames=NULL;//so no allocation
	params[0].midiValue=127;
	params[1].name="Mode";
	params[1].value=0;
	params[1].midiValue=0;
	params[1].list=6;//list with 6 items
	params[1].listNames=new string[6];//so we alocate an array of 6 strings
	params[1].listNames[0]="LP";
	params[1].listNames[1]="HP";
	params[1].listNames[2]="Peak Cut";
	params[1].listNames[3]="Peak Boost";
	params[1].listNames[4]="Notch";
	params[1].listNames[5]="BP";
	params[2].name="f";
	params[2].value=500;
	params[2].list=0;
	params[2].listNames=NULL;
	params[2].midiValue=0.025*127;	
	params[3].name="Q";
	params[3].value=10;
	params[3].list=0;
	params[3].listNames=NULL;
	params[3].midiValue=10;	
	params[4].name="Poles";
	params[4].value=0;
	params[4].midiValue=0;
	params[4].list=4;
	params[4].listNames=new string[4];
	params[4].listNames[0]="2";
	params[4].listNames[1]="4";
	params[4].listNames[2]="6";
	params[4].listNames[3]="8";
	params[5].name="Gain";
	params[5].value=1;
	params[5].list=0;
	params[5].listNames=NULL;
	params[5].midiValue=1/(float)10*127;
	
	buffer=new float[BUFF];
	//simple implementation of iir filter go on internet for more informations
	semiOrder=params[4].midiValue+1;
	Q=params[3].value;
	angularFreq=2*PI*params[2].value;
	angularFreq/=(float)SAMPLE_RATE; //normalized
	K=tan(angularFreq/2.);
	alpha=1+K;
	W=K*K;
	DE=1+K/Q+W;
	tapesl=new float*[semiOrder];	//for high order filter we simply chain 2order filters
	for(int i=0;i<semiOrder;i++)	//tapesl and tapesr are for left and right (STEREO)
	{
		tapesl[i]=new float[3];
		for(int j=0;j<3;j++)
			tapesl[i][j]=0;
	}
	tapesr=new float*[semiOrder];
	for(int i=0;i<semiOrder;i++)
	{
		tapesr[i]=new float[3];
		for(int j=0;j<3;j++)
			tapesr[i][j]=0;
	}
	computeCoef();
}

void IIRFilter::computeCoef()
{
	if(params[1].value==0)	//LP Cool, but do not set to much Q
	{
		a[0]=1;
		a[1]=2*(W-1)/DE;
		a[2]=(1-K/Q+W)/DE;
		b[0]=W/DE;
		b[1]=2*b[0];
		b[2]=b[0];
	}
	else if(params[1].value==1) //HP okay, but nasty fat bottom with low and mid-low cutoff 
	{
		a[0]=1;
		a[1]=2*(W-1)/DE;
		a[2]=(1-K/Q+W)/DE;
		b[0]=1./DE;
		b[1]=-2*b[0]*W;
		b[2]=b[0];
	}
	else if(params[1].value==2) //Peak Cut OK
	{
		 gain=exp((-params[5].value*0.115129254));
		float temp=1+K*(Q/gain)+W;
		a[0]=1;
		a[1]=4*(W-1)/temp;
		a[2]=(1-Q/gain+W)/temp;
		b[0]=(1+K/Q+W)/temp;
		b[1]=2*(W-1)/temp;
		b[2]=(1-K/Q+W)/temp;
		
	}
	else if(params[1].value==3)//peak boost  Fun
	//but I really like this messy one @low params like f=5,Q=5,Pole=4,gain=8, nasty!
	{
		float temp=1+K/Q+W;
		gain=exp(params[5].value*0.115129254);
		a[0]=1;
		a[1]=(1+K*gain/Q+W)/temp;
		a[2]=(1-K/Q+W)/temp;
		b[0]=(2*(1+K*gain/Q+W))/temp;
		b[1]=-2*(W-1)/temp;
		b[2]=(1-K*gain/Q+W)/temp;
		
	}
	else if(params[1].value==4) //Notch Not so good, but not so bad //3 10 4 12
	{
		alpha=sin(angularFreq)/(2.*Q);
		a[0]=1+alpha;
		a[1]=-2*cos(angularFreq);
		a[2]=1-alpha;
		b[0]=1;
		b[1]=-2*cos(angularFreq);
		b[2]=1;
		
	}
	else if(params[1].value==5) //BP Okay, need some gain
	{
		float temp=pow(10,params[5].value/20.);
		alpha=sin(angularFreq)/(2*Q);
		a[0]=1+alpha;
		a[1]=-2*cos(angularFreq);
		a[2]=1-alpha;
		b[0]=alpha*temp;
		b[1]=0;
		b[2]=-b[0];
		
	}
	if(a[2]*a[2]>=1)
		a[2]=0.99;
	bool ok =false;
	while(ok==false)	//okay it's kind of brutal, but we need to satisfy some conditions in order to have a stable filter
	{
		if(a[1]*a[1]>=(1+a[2])*(1+a[2]))
		{
			if(a[1]>(1+a[2]))
			a[1]-=0.0001;
			else
			a[2]+=0.0001;
		}
		else
			ok=true;
	}
	
}

void IIRFilter::run(float* input,int inputSize)
{
	//cout<<endl<<a[0]<<endl;
	mute.lock();	//setParameter() and run() are executed in two different threads, so be carefull when deleting/ reallocating pointers

	for(int i=0;i<inputSize;i++)
		buffer[i]=input[i];
	for(int i=0;i<semiOrder;i++)	//chaining filter
	{
		for(int j=0;j<inputSize;j++)
		{
			//left filter
			tapesl[i][2]=tapesl[i][1];
			tapesl[i][1]=tapesl[i][0];
			tapesl[i][0]=buffer[j]-(a[1]/a[0]*tapesl[i][1]+a[2]/a[0]*tapesl[i][2]);
			//buffer[j]=b[0]/a[0];
			buffer[j]=b[0]/a[0]*tapesl[i][0]+b[1]/a[0]*tapesl[i][1]+b[2]/a[0]*tapesl[i][2];
			j++;
			//right filter
			tapesr[i][2]=tapesr[i][1];
			tapesr[i][1]=tapesr[i][0];
			tapesr[i][0]=buffer[j]-(a[1]/a[0]*tapesr[i][1]+a[2]/a[0]*tapesr[i][2]);
			//buffer[j]=b[0]/a[0];
			buffer[j]=b[0]/a[0]*tapesr[i][0]+b[1]/a[0]*tapesr[i][1]+b[2]/a[0]*tapesr[i][2];
		}
	}
	mute.unlock();
	for(int i=0;i<inputSize;i++)
		input[i]=(1-params[0].value)*input[i]+ params[0].value*buffer[i];
}

void IIRFilter::modifyParameter(int param,int value)
{
	;
}
void IIRFilter::setParameter(int param,int value)
{
	if(param==0)	//DW
	{
		params[0].value=value/127.; //between 0 & 1
		params[0].midiValue=value;
	}
	else if(param==1)	//mode
	{
		params[1].value=value;
		params[1].midiValue=value;
		computeCoef();
	}
	else if(param==2) //f max=10k
	{
		params[2].value=value*10000/127.; //between à and 10000
		params[2].midiValue=value;
		angularFreq=2*PI*params[2].value;
		angularFreq/=(float)SAMPLE_RATE; //normalized
		K=tan(angularFreq/2.);	
		alpha=1+K;
		W=K*K;
		DE=1+K/Q+W;
		computeCoef();
	}
	else if(param==3) //Q
	{
		params[3].value=value;
		params[3].midiValue=value;
		Q=params[3].value;
		if(Q<1)
			Q=1;
		DE=1+K/Q+W;
		computeCoef();
	}
	else if(param==4) //order
	{
		params[4].value=value;
		params[4].midiValue=value;
		mute.lock();	//reallocation !!!
		for(int i=0;i<semiOrder;i++)
		{
			delete[] tapesl[i];
			delete[] tapesr[i];
		}
		if(semiOrder>1)
		{
			delete[] tapesl;
			delete[] tapesr;
		}
		else 
		{
			delete tapesl;
			delete tapesr;
		}
		semiOrder=params[4].midiValue+1;
		tapesl=new float*[semiOrder];
		for(int i=0;i<semiOrder;i++)
		{
			tapesl[i]=new float[2];
			for(int j=0;j<2;j++)
				tapesl[i][j]=0;
		}
		tapesr=new float*[semiOrder];
		for(int i=0;i<semiOrder;i++)
		{
			tapesr[i]=new float[2];
			for(int j=0;j<2;j++)
				tapesr[i][j]=0;
		}
		mute.unlock();
		computeCoef();
	}
	else if(param==5) //gain
	{
		params[5].value=value/127.*10;
		params[5].midiValue=value;
		computeCoef();
	}
}


IIRFilter::~IIRFilter()	//sometimes, segfault... not anymore.. I think...
{
	mute.lock();
	for(int i=0;i<semiOrder;i++)
	{
		delete[] tapesl[i];
		delete[] tapesr[i];

	}
	if(semiOrder>1)
	{
		delete[] tapesl;
		delete[] tapesr;
	}
	else 
	{
		delete tapesl;
		delete tapesr;
	}
	delete[] buffer;
	mute.unlock();
}
