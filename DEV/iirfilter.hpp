/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef _IIRFILTER_HPP_
#define _IIRFILTER_HPP_

#include "effect.hpp"
/*
	multimode IIR Filter
	with different orders achieve by chaining filters
*/

class IIRFilter : public Effect
{
	private :
	float** tapesl;	//left tapes delay, there is a buffer per filter in the chain
	float** tapesr;		//right tapes
	float* buffer;	//to copy the input buffer
	int semiOrder;	//order /2 'cause it's used for chaning second order filters together 
	float angularFreq;	//in fact not really angular freq ->normalized angular freq
	float alpha;	//variable to compute coefs
	float gain;	//same
	float W;	//same
	float Q;	//same
	float K;	//same
	float a[3];	//coef
	float b[3];	//feedback coef
	float DE;	//variable
	std::mutex mute;
	public :
	IIRFilter();
	void computeCoef();
	virtual void run(float* input, int inputSize)override; //callback
	virtual void modifyParameter(int param, int value)override;// add the value to one parameter
	virtual void setParameter(int param, int value)override;//set the value of one parameter
	~IIRFilter();
};
#endif
