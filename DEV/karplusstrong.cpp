/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "main.hpp"

#include "karplusstrong.hpp"
#include "utils.hpp"
using namespace std;

	KarplusStrong::KarplusStrong(int Knumber,int KbufferSize,std::mutex* KmutexVar):Voice("Karplus-Strong", Knumber, KbufferSize,KmutexVar,3/*number of params*/)	//name, voice id , buffer size, number of params
	{
		//first, init params
		params[0].name="Decay";
		params[0].value=0.99;
		params[0].list=0;//not a list
		params[0].listNames=NULL;//so no allocation
		params[0].midiValue=127*params[0].value;
		params[1].name="Metal";
		params[1].value=0.9;
		params[1].midiValue=127*params[1].value;
		params[1].list=0;//not a list
		params[1].listNames=NULL;
		params[2].name="C";
		params[2].value=0.5;
		params[2].midiValue=127*params[2].value;
		params[2].list=0;//not a list
		params[2].listNames=NULL;

		velocity=1;
		strongBufferSize=(int)SAMPLE_RATE/220.*2;//th buffer size is simply the period we want to our sound
		index=0;	//the index is used to navigate into the strong buffer
		strongBuffer= new float[strongBufferSize];
		for(int i=0;i<strongBufferSize;i++)
			strongBuffer[i]=0;	//no sound
 		
	}






	void KarplusStrong::excite(float freq,float vel)	//start note !
	{
		strongBufferSize=(int)SAMPLE_RATE/freq*2;	//@ the right freq
		velocity=vel;
		index=0;
		delete[] strongBuffer;
		strongBuffer = new float[strongBufferSize];
		for(int i=0;i<strongBufferSize;i++)
		{
			strongBuffer[i]=((rand()/(double)RAND_MAX)*2-1)*velocity; //random excitation
		}

	}


	void KarplusStrong::callback(float* outputBuffer)	//what the voice must do when portaudio wants some fresh data, return a buffer  lr lr lr lr
	{
		
		//the strong buffer has not the same sizez as the output buffer, hence the index
		for(int i=0;i<bufferSize;i++)
		{
			outputBuffer[i]=strongBuffer[(int)index];	//left
			i++;
			outputBuffer[i]=strongBuffer[(int)index];	//right
			index+=speed;//speed is set in voice.cpp, changed when pitchbend
			if(index>=strongBufferSize)	//end of the strong buffer, we need to repopulate it, see the karplus strong algo
			//karplus-stong +loss and decay
			{
				index=0;
				strongBuffer[0]=params[0].value*(1+params[1].value)*strongBuffer[0]/2.;
				for(int j=1;j<strongBufferSize;j++)
					strongBuffer[j]=params[0].value*((1-params[1].value)*strongBuffer[j-1]+(1+params[1].value)*strongBuffer[j])/2.;
      				//now an allpass filter for these fckng high frequencies, phase, etc...!!!!			
				
				simple_all_pass_filter(strongBuffer,strongBufferSize,params[2].value);	//simple all pass in utils.cpp
			}

		}	

	}

	void KarplusStrong::end() //endnote
	{
		;	//need to do something like a lp here
	}

	void KarplusStrong::modifyParameter(int param, int value)// add the value to one parameter //legacy, don't care
	{
		if(param==0)
		{
			float temp=params[0].value;
			temp+=value*0.01;
			if(temp>1)
				temp=1;
			if(temp<0.01)
				temp=0.01;
			params[0].value=temp;
			cout<<endl<<params[0].name<<" ="<<params[0].value<<endl;
			params[0].midiValue=127*params[0].value;
		}
		else if(param==1)
		{
			float temp=params[1].value;
			temp+=value*0.01;
			if(temp>1)
				temp=1;
			if(temp<0.01)
				temp=0.01;
			params[1].value=temp;
			cout<<endl<<params[1].name<<" ="<<params[1].value<<endl;
			params[1].midiValue=127*params[1].value;
		}
		else if(param==2)
		{
			float temp=params[2].value;
			temp+=value*0.01;
			if(temp>1)
				temp=1;
			if(temp<0.01)
				temp=0.01;
			params[2].value=temp;
			cout<<endl<<params[2].name<<" ="<<params[2].value<<endl;
			params[2].midiValue=127*params[2].value;
		}
	}

	void KarplusStrong::setParameter(int param, int value)//set the value of one parameter //midi (value = 8bits integer)
	{
		
		
		if(param==0)
		{	
				
			params[0].midiValue=value;	//we really REALLY need to do this
			params[0].value=value/127.0;	//between, 0 & 1
		}
		else if(param==1)
		{
			params[1].midiValue=value;
			params[1].value=value/127.0;	//between, 0 & 1
		}
		else if(param==2)
		{
			params[2].midiValue=value;
			params[2].value=value/127.0;	//between, 0 & 1
		}

	}

KarplusStrong::~KarplusStrong()
	{
		delete[] strongBuffer;	
	}
