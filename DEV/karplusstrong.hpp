/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _KARPLUSSTRONG_HPP_
#define _KARPLUSSTRONG_HPP_


#include "voice.hpp"
#include <cstring>
/*
	Karplus strong is a minimal string simulation
*/
using namespace std;

class KarplusStrong : public Voice
{
	private :
	float* strongBuffer;// our pseudo-digital-string
	float index;	//Where R We in Da Strong B*ff???	//float for pitch bend
	int strongBufferSize;//the size is set by the note we want to play (U know, period)
	float velocity;
	//three parameters of this voice :

	
	public :
	void debug()override{cout<<endl<<endl<<strongBufferSize<<endl<<endl;};
	KarplusStrong(int Knumber,int KbufferSize,std::mutex* KmutexVar);
	int returnSize()override{return 1;};
	void excite(float freq,float vel)override;	//start note !
	void callback(float* outputBuffer)override;	//what the voice must do when portaudio wants some fresh data, return a buffer
	void end()override; //endnote
	void modifyParameter(int param, int value)override;// add the value to one parameter
	void setParameter(int param, int value)override;//set the value of one parameter

	~KarplusStrong();

};

#endif
