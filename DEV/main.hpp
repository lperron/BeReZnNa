/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _MAIN_HPP_
#define _MAIN_HPP_

//pa defines
#define SAMPLE_RATE   (44100)
#define BUFF (128)	//pa buffer size
//pm defines
#define INPUT_BUFFER_SIZE 100
#define DRIVER_INFO NULL
#define TIME_PROC ((int32_t (*)(void *)) Pt_Time)
#define TIME_INFO NULL
#define MIDISPEED (1)	//ms, min =1
//#define TIME_START Pt_Start(1, 0, 0) /* timer started w/millisecond accuracy */
//other defines
#define NBVOICES (16) //with KS fine until 512 no eff @ 44100
#define MAJORVERSION ((int)0)
#define MINORVERSION ((int)4)
#define REVVERSION ((int)2)
#define STAGEVERSION ((const char *)" [DEV]")

#define PI (3.14156265)

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <chrono>
#include <ctime>
#include <pthread.h>
#include <unistd.h>
#include <termios.h>
#include <mutex>
#include <condition_variable>
#include <sstream>
#include <limits>
#include "portaudio.h"
#include "portmidi.h"
#include "porttime.h"

typedef struct
{
	std::string name;
	float value;
	int midiValue;
	int list;//0 if it's not a list
	std::string* listNames;
	
}
param;	//all the informations about one parameter (for a voice engine or an effect)

typedef struct
{
	int groupVoices;
	bool active;
	int min;	//for layer and split or semi layer/spit, the lowest and the highest notes for which our group will react;
	int max;
}
GroupData;	//informations about a group (voices manager)



#endif


