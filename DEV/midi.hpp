/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _MIDI_HPP_
#define _MIDI_HPP_

#include "main.hpp"
#include "voicesmanager.hpp"
#include "mixer.hpp"

typedef struct
{
	VoicesManager* Mannie;
	//Mixer* Mix;
	int channel;
	PmStream* midas;
	PmEvent buffer[1];
	void* Middle;	//a pointer the the our midi class

	//we will need a mutex here one day

}pmData;//the structure given to pm callback


typedef struct
{
	string name;
	string interface;
	int id;
	bool input;
	bool output;
}midiDevice;

typedef struct
{
	bool active;
	int CC;
	int someParam[2];	//for example setParameter will need the group + the param to be sent to the function //-1 if not 
	int module; //id for mixer or voice Manager
	int function; //id of the function
}midiRouting;

class Midi
{
	public :
	Midi(VoicesManager* Mmannie, Mixer* MMix);
	void changeDevice(int num);
	void refreshDevicesList();
	int returnNbDevices(){return Pm_CountDevices();};
	int returnInDevice(){return inDevice;};
	int returnChannel(){return midiInfo.channel;};
	void setChannel(int i){midiInfo.channel=i;};
	void controlChange(int control,int value);
	midiDevice* returnDevices(){return devices;};
	void armMapping(int module, int function,int param1,int param2);
	void unArm(){mapping=false;};
	void unMapSetP(int module,int param1);	//mixer or voicesm and wich group or effect;
	void unMapAll();	//unmap all routing except default
	void unMap(int module, int function,int param1,int param2);//unmap one routing
	void defaultCCMapping();//basic things like sustain
	void CCSwitch(int i,int value);//redistribute CC to functions according to mapping
	~Midi();
	private :
	Mixer* Mix;
	pmData midiInfo;
	midiDevice* devices;
	midiRouting map[512];
	bool mapping=false;
	int slotSelected=0;
	//VoicesManager* Mannie;
	//PmStream * midi;
    	// int length;
    	//PmEvent buffer[1];
	int inDevice;
	
};


#endif
