/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "mixer.hpp"

using namespace std;

static int callback( const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData)
//this is the pa callback, it's called when pa need a new buffer, the mixer is then sync to this callback
{
//std::lock_guard<std::mutex> lockGuard(mutexvar);
	paData *data = (paData*)userData;
	float *out = (float*)outputBuffer;
	(void) inputBuffer; /* Prevent unused variable warning. */
	std::unique_lock<std::mutex> l(data->mute);	//we don't want this thread to take a buffer wich is not ready, so wait until the mixer has done his job
	while(data->hungry==true && data->terminate==false)
	{
	data->dataReady.wait_for(l,std::chrono::seconds(1));
	}
	for(unsigned int j=0;j<framesPerBuffer;j++)
	{

		*out++=data->buffer[2*j];
		*out++=data->buffer[j*2+1];
	}
	data->hungry=true;
	l.unlock();
	if(data->terminate==false);
	data->needData.notify_one();	//tell the mixer it should provide some fresh data
	return 0;
		
}	


Mixer::Mixer(int MvoicesNumber,float Mvolume,int MbufferSize,Voice*** Mvoices, std::mutex* MmutexVar)//constructor	+ pa init
{
	mutexVar=MmutexVar;

	volume=Mvolume;
	voicesNumber=MvoicesNumber;
	voices=*Mvoices;
	bufferSize=BUFF;
	voiceVolume=new float[NBVOICES];
	for(int i=0;i<NBVOICES;i++)
		voiceVolume[i]=1;
	mixerInfo.buffer=new float[bufferSize];	//this is the buffer used by port audio
	for(int i=0;i<bufferSize;i++)
		mixerInfo.buffer[i]=0;	//no sound
	mixerInfo.hungry=true;	//pa can now take this buffer
	tempBuffer=new float[bufferSize];
	err = Pa_Initialize();	//do something with err
	if( err != paNoError )
		cout<<endl<<endl<<Pa_GetErrorText(err)<<endl<<endl; 
	err = Pa_OpenDefaultStream( &stream,0,/* no input channels */2,/* stereo output */paFloat32,SAMPLE_RATE,BUFF/2,callback,&mixerInfo );//we init the pa stream with our callback 
	 if( err != paNoError )
		cout<<endl<<endl<<Pa_GetErrorText(err)<<endl<<endl;
	 err = Pa_StartStream( stream );
	 if( err != paNoError )
		cout<<endl<<endl<<Pa_GetErrorText(err)<<endl<<endl;
	Effects=new Effect*[numberEffects];	//the number of effects can be modified without any problem
	for(int i=0;i<numberEffects;i++)	//but for now, only 2
	{
		if(i==0)
			Effects[i]=initEffect(0);//phaser
		else
			Effects[i]=initEffect(1);//disto
	}
	

}

void Mixer::mix()	//this is the central function of the audio path, it asks each voice data to fill the buffer, mixes them and sends it to effects, then notify the pa callback 
{

		 std::unique_lock<std::mutex> l(mixerInfo.mute);
		while(mixerInfo.hungry==false)	//sync with the pa callback, cpu-friendly and errorless
		{
		mixerInfo.needData.wait(l);
		}
		mutexVar->lock();	//we don't want a voice to be excited when I look at it buffer
		//need to collect buffers
		voices[0]->callback(tempBuffer);
		//and mix
		for(int i=0;i<bufferSize;i++)
			mixerInfo.buffer[i]=tempBuffer[i]*voiceVolume[0];
		//same for all the other voices
		for(int i=1;i<voicesNumber;i++)
		{

			voices[i]->callback(tempBuffer);
			for(int j=0;j<bufferSize;j++)
			{	
				
				mixerInfo.buffer[j]+=tempBuffer[j]*voiceVolume[i];
	
			}	
		}
		mutexVar->unlock();	//now voices can be excited

		//now we set the volume
		float tempvol=volume/(float)(voicesNumber);//divide by nbvoices to avoid clipping when gain is set to 1
		for(int i=0;i<bufferSize;i++){
			mixerInfo.buffer[i]*=tempvol;
		}
		//audio processing by effects
		for(int i=0;i<numberEffects;i++)
		{
			if(Effects[i]->isOn())
			Effects[i]->run(mixerInfo.buffer,bufferSize);
		}
		mixerInfo.hungry=false;
		mutexVar->unlock();
		l.unlock();
		mixerInfo.dataReady.notify_one();	//go pa go
}

void Mixer::setVolume(float Mvolume)
{
	volume=Mvolume;
}

int Mixer::returnEffectNumberParam(int EffectNum)
{
	return Effects[EffectNum]->returnNumberParams();
}

string Mixer::returnEffectName(int EffectNum)
{
	return Effects[EffectNum]->returnName();
}
param* Mixer::returnEffectParams(int EffectNum)
{
	return Effects[EffectNum]->returnParams();
}
void Mixer::setEffectParameter(int ef,int para,int value)
{
	Effects[ef]->setParameter(para,value);
}

bool Mixer::returnEffectActive(int EffectNum)
{
	return Effects[EffectNum]->isOn();
}

void Mixer::EffectToggle(int EffectNum)
{
	Effects[EffectNum]->on();
}

void Mixer::changeEffect(int eff,int effeng)	//changes = delete + new
{
	if(eff<numberEffects and effeng<NB_EFFECT_ENGINES)
	{
		bool temp=Effects[eff]->isOn();	//we want to know if the effect is on or not
		Effects[eff]->on();
		delete Effects[eff];
		Effects[eff]=initEffect(effeng);	//init effect is in alleffect.cpp it is a switch of constructors, returns a new effect
		if(temp)	//we put the effect back in the right state
		{
			Effects[eff]->on();
		}
		loadEffectPreset(0,eff);
	}
}

void Mixer::loadEffectPreset(int preset,int eff)
{
	Effects[eff]->loadPreset(preset);
}
void Mixer::saveEffectPreset(int preset,string presetName,int eff)
{
	Effects[eff]->savePreset(preset,presetName);
}
string Mixer::getEffectPresetName(int preset,int eff)
{
	return Effects[eff]->getPresetName(preset);
}
Mixer::~Mixer()
{

	delete[]  mixerInfo.buffer;
	delete[] voiceVolume;
	delete [] tempBuffer;
	for(int i=0;i<voicesNumber;i++)
		delete voices[i];
	delete[] voices;
	for(int i=0;i<numberEffects;i++)
		delete Effects[i];
	delete[] Effects;
}
