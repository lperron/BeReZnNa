/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _MIXER_HPP_
#define _MIXER_HPP_
#include "main.hpp"
#include "voice.hpp"
#include "effect.hpp"
#include "alleffects.hpp"

using namespace std;

typedef struct
{

	float* buffer;	//the sound

	bool hungry;	//do we need another fresh buffer
	std::condition_variable needData;	//two condition variable for threads sync
	std::condition_variable dataReady;
	std::mutex mute;	//mutex for sync and lock
	bool terminate=false;
}
paData;	//the structure given to pa callback


class Mixer
{
	
	private :	
	paData mixerInfo;	//given to callback, please take a look to paData struct
	PaStream *stream;	
	PaError err;
	float* tempBuffer;	//the buffer used to mix data from voices
	int bufferSize;
	float volume;	//main volume
	float* voiceVolume;	//voice volume, for mixing
	int voicesNumber;
	Voice** voices;	//all our voices are in this dynamic array
	std::mutex* mutexVar;	//to lock voices buffer (to avoid a voice excitation when we're looking at its buffer)
	Effect** Effects;	//all our effects
	int numberEffects=2;	//to change the number of effects you just have to change this variable
	int* group;
	
	public :
	Mixer(int MvoicesNumber,float Mvolume,int MbufferSize,Voice*** Mvoices,std::mutex* MmutexVar);
	void mix();	//super important!!!
	//well, all these functions are explicit
	void setVolume(float Mvolume);
	void setVoiceVolume(int i,float vol){voiceVolume[i]=vol;};
	float returnVoiceVolume(int i){return voiceVolume[i];};
	int returnVolume(){return volume;};
	int returnNumberEffects(){return numberEffects;};
	void EffectToggle(int EffectNum);
	bool returnEffectActive(int EffectNum);
	int returnEffectNumberParam(int EffectNum);
	string returnEffectName(int EffectNum);
	param* returnEffectParams(int EffectNum);
	void setEffectParameter(int ef,int para,int value);
	void changeEffect(int eff,int effeng);
	void loadEffectPreset(int preset, int eff);
	void saveEffectPreset(int preset,string presetName,int eff);
	string getEffectPresetName(int preset,int eff);
	void stopPa(){mixerInfo.terminate=true;err = Pa_CloseStream( stream );Pa_Terminate();};
	~Mixer();
};

#endif
