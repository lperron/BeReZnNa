/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "phaser.hpp"


Phaser::Phaser():Effect("Phaser",6)//effect name, nb params
{

	//init param
	params[0].name="Dry/Wet";
	params[0].value=0.5;
	params[0].midiValue=127*0.5;
	params[0].list=0;	//not a list
	params[0].listNames=NULL;//so no allocation
	params[1].name="Stages";
	params[1].value=8;
	params[1].midiValue=2;
	params[1].list=5;	//list of five elements
	params[1].listNames=new string[5];	//so allocation of 5 strings
	params[1].listNames[0]="2";
	params[1].listNames[1]="4";
	params[1].listNames[2]="8";
	params[1].listNames[3]="16";
	params[1].listNames[4]="32";
	params[2].name="C";	//between -1 & 1
	params[2].value=0.5;	//=value/127.*2-1
	params[2].midiValue=0.5*127/2.+1;
	params[2].list=0;
	params[2].listNames=NULL;
	params[3].name="Wave";
	params[3].value=0;
	params[3].midiValue=0;
	params[3].list=5;//list of five elements
	params[3].listNames=new string[5];//alloc
	params[3].listNames[0]="sine";
	params[3].listNames[1]="ramp";
	params[3].listNames[2]="triangle";
	params[3].listNames[3]="pulse";
	params[3].listNames[4]="noise";
	params[4].name="LFO Freq";
	params[4].value=2;//0.1*value^2
	params[4].midiValue=sqrt(20);
	params[4].list=0;
	params[4].listNames=NULL;
	params[5].name="LFO Amp";
	params[5].value=1;
	params[5].midiValue=127;
	params[5].list=0;
	params[5].listNames=NULL;

	lfoTabSize=SAMPLE_RATE/(params[4].value); //the lfo is just a tab
	lfo=new float[lfoTabSize];
	generateLFO();
	lfoIndex=0;
}
void Phaser::generateLFO()
{
	if(params[3].value==0)	//sine
	{	float temp=2*PI/(float)lfoTabSize;
		for(int i=0;i<lfoTabSize;i++)
			lfo[i]=sin(i*temp)*params[5].value;
	}
	if(params[3].value==1)//ramp
	{
		for(int i=0;i<lfoTabSize;i++)
			lfo[i]=i/(float)lfoTabSize*params[5].value;
	}
	if(params[3].value==2)//triangle
	{	
		
		float temp=0;
		for(int i=0;i<lfoTabSize;i++)
		{
			if(i<lfoTabSize/2.)
				temp+=1*2/(float)lfoTabSize;
			else
				temp-=1*2/(float)lfoTabSize;
			lfo[i]=temp*params[5].value;

		}
	}
	if(params[3].value==3)//pulse
	{
		for(int i=0;i<lfoTabSize;i++)
		{
			if(i<lfoTabSize/2.)
				lfo[i]=params[5].value;
			else 
				lfo[i]=-params[5].value;
		}
	}
	if(params[3].value==4)//noise
	{
		for(int i=0;i<lfoTabSize;i++)
		{
			lfo[i]=rand()/(float)RAND_MAX*params[5].value;
		}
	}
}
void Phaser::run(float* buffy,int bufferSize)//data processing
{
	float* tempBuffer=new float[bufferSize];
	for(int i=0;i<bufferSize;i++)
		tempBuffer[i]=buffy[i];
	//a phaser is basically a chain of all pass filters
	for(int i=0;i<params[1].value;i++)//for each stage
	{
		float templ=0;	//as usual, we need to do the job twice, left and right
		float beforel=0;
		float tempr=0;
		float beforer=0;
		int Ctemp=0;
		for(int i=0;i<bufferSize;i++)	
		{

			Ctemp=params[2].value+lfo[lfoIndex];	//the lfo changes the value of C
			if(Ctemp>1)
				Ctemp=1;
			if(Ctemp<-1)
				Ctemp=-1;
			beforel=buffy[i];
			beforer=buffy[i+1];
			buffy[i]=templ+Ctemp*beforel;
			templ=beforel-Ctemp*buffy[i];
			buffy[i+1]=tempr+Ctemp*beforer;
			tempr=beforer-Ctemp*buffy[i+1];
			if(i%100==0)	//brutal, slow down the lfo by 100, to have a slow LFO without having a too huge LFO array
				lfoIndex++;
			if(lfoIndex>=lfoTabSize)	//end of the lfo, going back to zero
			{
				
				lfoIndex=0;
				if(params[3].value==4)		//if noise, we need to repopulate the lfo
				{
					for(int i=0;i<lfoTabSize;i++)
					{
						lfo[i]=rand()/(float)RAND_MAX*params[5].value;
					}
				}
				
			}
			i++;
			

		}
	}
	for(int i=0;i<bufferSize;i++)
	{
		//transmit the processed data, with dry wet factor
		buffy[i]*=params[0].value;
		buffy[i]+=(1-params[0].value)*tempBuffer[i];
	}
	delete[] tempBuffer;
}

void Phaser::modifyParameter(int param,int value)//legacy, don't care
{
	/*if(param==0)//DryWet
	{
		float temp =params[0].value+value*0.01;
		if(temp>1)
			temp=1;
		if(temp<0)
			temp=0;
		params[0].value=temp;
		cout<<endl<<params[0].name<<" = "<<temp;
	}
	else if(param==1)//stages
	{
		float temp =params[1].value+value;
		if(temp>32)
			temp=32;
		if(temp<1)
			temp=1;
		params[1].value=temp;
		cout<<endl<<params[1].name<<" = "<<temp;
	}
	else if(param==2)//C
	{
		float temp =params[2].value+value*0.02;
		if(temp>1)
			temp=1;
		if(temp<-1)
			temp=-1;
		params[2].value=temp;
		cout<<endl<<params[2].name<<" = "<<temp;
	}
	else if(param==3)//lfo wave
	{
		int temp =params[3].value+value;
		if(temp>4)
			temp=4;
		if(temp<-0)
			temp=0;
		params[3].value=temp;
		cout<<endl<<params[3].name<<" = "<<params[3].listNames[temp];
		generateLFO();
	}
	else if(param==4)//lfo freq
	{
		float temp =params[4].value+value*0.2;
		if(temp>8000)
			temp=1;
		if(temp<0.2)
			temp=0.2;
		params[4].value=temp;
		cout<<endl<<params[4].name<<" = "<<temp;
		delete[] lfo;
		lfoTabSize=SAMPLE_RATE/(params[4].value);
		lfo=new float[lfoTabSize];
		generateLFO();
		lfoIndex=0;
	}
	else if(param==5)//lfo Amp
	{
		float temp =params[5].value+value*0.01;
		if(temp>1)
			temp=1;
		if(temp<0)
			temp=0;
		params[5].value=temp;
		cout<<endl<<params[5].name<<" = "<<temp;
		generateLFO();
	}*/
}
void Phaser::setParameter(int param,int value)
{
	if(param==0)	//dry wet
	{
		params[0].value=value/(float)127;
		params[0].midiValue=value;
	}
	else if(param==1)	//stages
	{
		int temp=value;
		if(value>=0 && value<5)
		{
			int tempb;
			if(value==0)
				tempb=2;	//remember that autoGui return only the index of the element selected; so for 2 stages it's zero
			else if(value==1)
				tempb=4;
			else if(value==2)
				tempb=8;
			else if(value==3)
				tempb=16;
			else if(value==4)
				tempb=32;
		params[1].value=tempb;
		params[1].midiValue=temp;			
		}
	}
	else if(param==2)	//C
	{
		float temp=value/127.*2-1;
		params[2].value=temp;
		params[2].midiValue=value;
	}
	else if(param==3)//lfo wave
	{
		int temp =value;
		if(temp>=0 && temp<5)
		{
			params[3].value=temp;
			params[3].midiValue=temp;
			generateLFO();
		}
	}
	else if(param==4)//lfo speed
	{
		
		float temp=0.1*value*value;
		if(temp<0.1)
			temp=0.1;
		params[4].value=temp;
		params[4].midiValue=value;
		lfoTabSize=SAMPLE_RATE/(params[4].value);
		delete[] lfo;	
		lfo=new float[lfoTabSize];
		generateLFO();
		lfoIndex=0;
	}
	else if(param==5)//lfo Amp
	{
		params[5].value=value/127.;
		params[5].midiValue=value;
		generateLFO();
	}
}

Phaser::~Phaser()
{
	delete[] lfo;
}
