/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "voice.hpp"

using namespace std;



Voice::Voice(string Vname,int Vnumber,int VbufferSize,std::mutex* VmutexVar,int nbparam)	//basic constructor of a Voice, nothing really interesting here
{

	numberParams=nbparam;
	params=new param[numberParams];//PARAMS are allocated here, but not list name
	name=Vname;
	number=Vnumber;
	bufferSize=VbufferSize;
	mutexVar=VmutexVar;
	presetName="Default";
}

void Voice::pitchBend(float ispeed)
{
	speed=ispeed;
	//ispeed is set in voice manager
}
void Voice::loadPreset(int preset)
{
	if(preset<128)
	{
		string bankName="./Presets/"+name+".blob";
		string line;
		ifstream fpreset(bankName.c_str(),ios::in);
		if(fpreset)
		{
			for(int i=0;i<=preset;i++)
				getline(fpreset,line);
			istringstream temp(line);
			string bin;
			temp>>bin;	//preset name,  params 0 .value, params 0.midiValue,...
			for(int i=0;i<numberParams;i++)
				temp>>params[i].value>>params[i].midiValue;
			fpreset.close();
		}
		else
		{
			createBank();
		}	

	}
}

string Voice::getPresetName(int preset)
{
	if(preset<128)
	{
		string bankName="./Presets/"+name+".blob";
		string line;
		string name;
		ifstream fpreset(bankName.c_str(),ios::in);
		if(fpreset)
		{
			for(int i=0;i<=preset;i++)
				getline(fpreset,line);
			istringstream temp(line);
			string bin;
			temp>>name;	//preset name,  params 0 .value, params 0.midiValue,...
			for(int i=0;i<numberParams;i++)
				temp>>bin>>bin;
			fpreset.close();
			return name;
		}
		else
		{
			createBank();
			return "default";
			
		}	

	}
	return "None";
}
void Voice::savePreset(int preset,string presetName)
{
		//int i=0;
		string bankName="./Presets/"+name+".blob";
		string line;
		string tempBank;
		ostringstream stempBank;
		ifstream fpreseta(bankName.c_str(),ios::in);
		if(fpreseta)
		{
			for(int i=0;i<128;i++)
			{
				getline(fpreseta,line);
				if(preset!=i)
					stempBank<<line<<endl;
				else
				{
					stempBank<<presetName<<" ";
					for(int j=0;j<numberParams;j++)
						stempBank<<params[j].value<<" "<<params[j].midiValue<<" ";
					stempBank<<endl;
				}
			}
			fpreseta.close();
		}
		tempBank=stempBank.str();
		ofstream fpreset(bankName.c_str(),ios::out | ios::trunc);
		if(fpreset)
		{
			fpreset<<tempBank;
			fpreset.close();
		}
}

void Voice::createBank()
{
		string bankName="./Presets/"+name+".blob";
		string presetName="Default";
		ofstream fpreset(bankName.c_str(),ios::out | ios::trunc);
		if(fpreset)
		{
			for(int i=0;i<128;i++)
			{
				fpreset<<presetName<<" ";
				for(int j=0;j<numberParams;j++)
					fpreset<<params[j].value<<" "<<params[j].midiValue<<" ";
				fpreset<<endl;
			}
			fpreset.close();
		}
}
Voice::~Voice()
{
		for(int i=0;i<numberParams;i++)
		{
			if(params[i].listNames)
				delete[] params[i].listNames;
		}
	delete[] params;

}
/*
void voice 
it really simplifies the rest of BeReZnNa
so we can declare a voice, and call it callback without waisting computationnal time
*/
VoidVoice::VoidVoice(int Knumber,int KbufferSize,std::mutex* KmutexVar):Voice("VoidVoice", Knumber, KbufferSize,KmutexVar,1/*number of params*/)
{
	params[0].name="None";
	params[0].value=0;
	params[0].list=0;//not a list
	params[0].listNames=NULL;
	params[0].midiValue=0;

	
}
VoidVoice::~VoidVoice()
{
	;
}

