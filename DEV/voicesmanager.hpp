/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _VOICESMANAGER_HPP_
#define _VOICESMANAGER_HPP_
#include "main.hpp"
#include "voice.hpp"
#include "allvoices.hpp"

using namespace std;
class VoicesManager
{
	private	:
	int nbVoices;//how many voices
	bool* assigned;	//is this voice already assigned (note on)
	bool* selected;	//selected ->legacy
	int* group; //if -1 voice not assigned, else the index of the group
	int numberGroup;	//how many group do we have
	int nbFreeVoices;	//how many voices are group-free (void voice)
	GroupData* groupInfo;	//see main.hpp
	std::mutex* mutexVar;	//to lock when excite (to avoid mix and excite at the same moment, see mixer.cpp)
	clock_t* time;//when the state (note on, note off) changed, used for priority order
	//maybe something for custom scale;
	int *note;	//the note played by a voice
	float midiToHz[128];	//8bits integer to frequency
	Voice** voices;	//all our voices
	float* tuning;	//for scl scales
	float freq=440; //for scl scales
	int ref=69; //for scl scales
	int first=0;//for scl scales
	int last=127;//for scl scales
	int notesPerOctave=-1; //for scl scales
	bool noodle=true;	//scl or noodle
	bool range=1;
	int* sostenustain;	//0=nada, 1 sustain
				//when sustain pedal is released, all sustained notes are released
	bool sustain=false;
	bool sostenuto=false;
	

	public :
	VoicesManager(int VnbVoices,Voice*** Vvoices,std::mutex* VmutexVar);
	//explicit 
	int returnGroupNumberParam(int Vgroup);
	int returnNbFreeVoices(){return nbFreeVoices;};
	string returnGroupName(int Vgroup);
	param* returnGroupParams(int Vgroup);
	void noteOn(int Vnote,float Vvelocity);
	void noteOff(int Vnote);
	void sustainPedal(int value);
	void sostenutoPedal(int value);
	void modifyParameter(int param, int value);
	void setParameter(int para,int value);
	void setGroupParameter(int gro,int para,int value);
	void loadPreset(int preset,int gro);
	void savePreset(int preset, string presetName,int gro);
	string getPresetName(int preset,int gro);
	void addGroup(int gro,int voice,int eng,int imin,int imax);
	void deleteGroup(int gro);
	void modifyGroup(int gro,int voice,int eng);
	void changeScale(string file);	//load the selected scale
	
	void loadScl(string file);
	void setRef(int refe);
	void setFreq(float frequ);
	int returnRef(){return ref;};
	float returnFreq(){return freq;}; 
	void updatePitch();
	int returnVoiceGroup(int i){return group[i];};
	void pitchBend(int value);
	GroupData* returnGroupInfo(){return groupInfo;};
	~VoicesManager();
	

};
#endif
