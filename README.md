# BeReZnNa

<p align='center'>c++ DSP engine for fast prototyping</p>
<p align='center'>It will allow you to focus on the DSP code without having to handle Midi protocol, polyphony, GUI, mixing and many other things. BeReZnNa will do it for you! Eventually, to make an instrument, you only have to fill a floats buffer corresponding to one voice when BeReZnNa asks it.</p>
<p align='center'>For example the additive voice, with its 8 sines and its ADSR env, is writed in less than 200 lines, but as BeReZnNa engine handles almost everything, it is polyphonic, it supports microtuning, midi mapping, presets, can be split or layered with other voices, etc...</p>

## Dependencies
- gtkmm-3.0
- libasound2-dev



## Installation & Compilation

- 1.Stable

```
~$ git clone https://gitlab.com/lperron/BeReZnNa.git
~$ cd BeReZnNa/stable/
~$ ./configure
~$ make
~$ sudo make install
```
- 2.Dev

```
~$ git clone https://gitlab.com/lperron/BeReZnNa.git
~$ cd BeReZnNa/DEV/
~$ make
```

## Screenshots

BeReZnNa Mixer:
<p align='center'><img src='https://gitlab.com/lperron/BeReZnNa/raw/master/Screenshots/bereznnaMix.png'</p>

Karplus-Strong Engine:
<p align='center'><img src='https://gitlab.com/lperron/BeReZnNa/raw/master/Screenshots/bereznnaKS.png'</p>

Additive Engine:
<p align='center'><img src='https://gitlab.com/lperron/BeReZnNa/raw/master/Screenshots/bereznnaAdd.png'</p>

IIR Filter:
<p align='center'><img src='https://gitlab.com/lperron/BeReZnNa/raw/master/Screenshots/bereznnaIIR.png '</p>