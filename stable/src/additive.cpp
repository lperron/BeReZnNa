/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "main.hpp"

#include "additive.hpp"
#include "utils.hpp"
using namespace std;

	Additive::Additive(int Knumber,int KbufferSize,std::mutex* KmutexVar):Voice("Additive", Knumber, KbufferSize,KmutexVar,12/*number of params*/)
{
		for(int i=0;i<nbCoef+4;i++)	//initialization of the parameters
		{

			params[i].value=0;	//value used by the voice
			params[i].list=0;//not a list -> so it will be a slider
			params[i].listNames=NULL;//not a list so no names (for lists examples, see phaser, disto, IIR, FIR)
			params[i].midiValue=0;	//midi value, the value of the parameter used for communication (midi or with the GUI)
			if(i<nbCoef)
				params[i].name=to_string(i-2);	//the name displayed by the GUI
			else if(i==nbCoef)
				params[i].name="A";	//the name displayed by the GUI
			else if(i==nbCoef+1)
				params[i].name="D";	//the name displayed by the GUI
			else if(i==nbCoef+2)
			{
				params[i].name="S";	//the name displayed by the GUI
				params[i].value=1;
				params[i].midiValue=127;
			}
			else if(i==nbCoef+3)
			{
				params[i].name="R";	//the name displayed by the GUI
				params[i].value=SAMPLE_RATE*20*6/127.*6/127.*6/127.;
				params[i].midiValue=6;
			}
		}
		params[nbCoef].value=0;	//attack time (samples)
		params[nbCoef+1].value=0;	//decay time
		params[nbCoef+2].value=1;			//sustain lvl
		params[nbCoef+3].value=SAMPLE_RATE*2/127.;	//release time

		params[2].midiValue=127;	//Params[2] is the fundamental sine
		params[2].value=1;		//so, by default, it's the only non-zero element
		f=440;	//because we must
	 	addBuffSize=4*SAMPLE_RATE/(f);	//our buffer
		index=0;
		buffer=new float[addBuffSize];
		velocity=0;
		for(int i=0;i<addBuffSize;i++)
			buffer[i]=0;	//no sound
}

void Additive::excite(float freq,float vel)
{
	envIndex=0;
	envStage=0;
	velocity=vel;
	f=freq;
	index=0; //we reset our wave
	delete[] buffer;	//new note=new freq= new buffer size
	addBuffSize=4*SAMPLE_RATE/(f);	//4 is because the lowest freq is sub2 so freq/4
	buffer=new float[addBuffSize];
	float tempb=f*2.*PI/(float)SAMPLE_RATE;	//just to not compute this at each iteration
	for(int i=0;i<addBuffSize;i++) //we fill our buffer
	{
		float temp=0;// gonna be the sum of all the sines*coef

		temp+=params[0].value*sin(i/4.*tempb);	//sub2
		temp+=params[1].value*sin(i/2.*tempb);		//sub1
		temp+=params[2].value*sin(i*tempb);	//fond
		for(int j=3;j<nbCoef;j++)
			temp+=params[j].value*sin(i*pow(2,(j-2))*tempb);	//all the other sines
		temp/=(float)nbCoef;	//normalization to avoid potential clipping
		temp*=velocity;	
		buffer[i]=temp;		
	}
}

void Additive::callback(float *outputBuffer)	//the less we do here, the best it is -> !!!CRITICAL!!! -> CALLBACK -> Called for each voice at 2*SAMPLE_RATE/BUFF Hz

{
	for(int i=0;i<bufferSize/2;i++)//stereo output buffer, but not stereo engine
	{
		//first we compute the lvl as a function of the ADSR
		envIndex++; //where R we in the current stage
		if(envStage==0)//attack
		{
			lvl=envIndex/(params[nbCoef].value+1);
			if(envIndex>=params[nbCoef].value)	//well time to move to the next stage
			{
				envStage=1;
				envIndex=1;
			}
		}
		else if(envStage==1)//decay
		{
			lvl=1-(envIndex/(params[nbCoef+1].value+1)*(1-params[nbCoef+2].value));
			if(envIndex>=params[nbCoef+1].value)	//now it's time for sustain
			{
				envStage=2;
				envIndex=1;
			}
		}
		else if(envStage==2)
		{
			lvl=params[nbCoef+2].value;
		}
		else if(envStage==3)
		{
			lvl=relLvl*(1-envIndex/(params[nbCoef+3].value+1));
			if(envIndex>=params[nbCoef+3].value)//lvl=0, go to next stage
			{
				envStage=4;
				envIndex=1;
			}
		}
		 else if(envStage==4)	//end
		{
			lvl=0;
		}
		outputBuffer[2*i]=buffer[(int)index]*lvl;	//so we set the same value to L
		outputBuffer[2*i+1]=buffer[(int)index]*lvl;	//and R
		index+=1*speed;	//next value of our buffer	//speed is set in voice.cpp
		if(index>=addBuffSize)	//this is the end
			index=0;	//my only friend
	}				//the end
}

void Additive::end()	//one can simply set all to zero, but, with this way we can easily improve the engine to add release time, and this function is not critical so DON'T CARE
{
	velocity=0;
	relLvl=lvl;
	envStage=3;
	envIndex=1;
}

void Additive:: modifyParameter(int param,int value) 	//legacy, don't really care
{
	;
}

void Additive:: setParameter(int param,int value)
{
	if(param>=0 && param<nbCoef)
	{
		float diff=value/127.-params[param].value;
		params[param].value=value/127.;	//we need to normalize the value, in this case the value must be between 0 and 1

		params[param].midiValue=value; //we always need to do this, ALWAYS (midiValue can be set by multiple sources, Gui or midi, and only the voice knows all these operations, so it must keep this midiValue fresh)
		//we're going to refresh the buffer
		float tempb=f*2.*PI/(float)SAMPLE_RATE;		//used in each iteration
		float tempp=4-param*2;				//idem
		if(tempp<=0)
			tempp=1;
		for(int i=0;i<addBuffSize;i++)	
		{
			if(param<3)
			{

				buffer[i]+=diff*velocity*sin(i/tempp*tempb);
			}
			else
				buffer[i]+=diff*velocity*sin(i*pow(2,(param-2))*tempb);//clean way	
		}
	}
	else if(param==nbCoef || param==nbCoef+1 || param==nbCoef+3)
	{
		params[param].midiValue=value;
		params[param].value=SAMPLE_RATE*20*value/127.*value/127.*value/127.;
	}

	else if(param==nbCoef+2)
	{
		params[param].midiValue=value;
		params[param].value=value/127.;
	}
}
