/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _ADDITIVE_HPP_
#define _ADDITIVE_HPP_

/*
Additive is a simple additive engine
just a sum of sines
the user can choose the weight of each sine to achieve the desired harmony
*/
#include "voice.hpp"
#include <cstring>
using namespace std;

class Additive : public Voice
{
	private :
	float index; //for buffer, (the buffer of the engine has not the same size as the port audio buffer, so we need to keep in mind where we are in our buffer till the next callback) //float for cheap pitch bend !!!
	float velocity;
	int nbCoef=8; //how many sines, if you change this parameter you must also change the number of params in the call to voices constructor
	float f; //frequency of the note we want to play
	int addBuffSize; //the size of our buffer, dependant of the freq
	float* buffer;	//and our buffer
	float relLvl;//the release lvl -> the lvl when the key is released
	int envStage=4; //where are we now? 0=attack 1=decay 2=sustain 3=release 4=end 
	int envIndex; //incremented at each sample
	float lvl=0; //the level of the voice (adsr)

	public :
	void debug()override{;};	//not usefull if the voice is OK
	Additive(int Knumber,int KbufferSize,std::mutex* KmutexVar);
	int returnSize()override{return 1;};	//size= is it bad for my cpu or not? 1=normal , 2=not so good so let's consider this voice like two voices, etc... not usefull for the moment but maybe some day..
	void excite(float freq,float vel)override;	//start note !
	void callback(float* outputBuffer)override;	//what the voice must do when portaudio wants some fresh data, return a buffer
	void end()override; //endnote
	void modifyParameter(int param, int value)override;// add the value to one parameter  // legacy
	void setParameter(int param, int value)override;//set the value of one parameter
	~Additive(){delete[] buffer;};

};

#endif
