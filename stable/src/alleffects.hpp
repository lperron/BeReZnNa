/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _ALLEFFECTS_HPP_
#define _ALLEFFECTS_HPP_
/*
this header (and its .cpp) references all the effects used by BeReZnNa, so to add a new effect you only have to edit this file(and its .cpp))
*/
#define NB_EFFECT_ENGINES (4)	//to add an effect please increment this value

#include "phaser.hpp"
#include "disto.hpp"
#include "firfilter.hpp"
#include "iirfilter.hpp"
//to add an effect, please add his header here

std::string returnEffectEngineName(int i);//see .cpp
Effect* initEffect(int eng);//see .cpp
#endif
