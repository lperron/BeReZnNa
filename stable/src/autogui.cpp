/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "autogui.hpp"

bool mapping=false;
bool unmapping=false;
//class PresetDial////////////////////////////////////////////////////
PresetDial::PresetDial(Gtk::Window* parent) : Gtk::Dialog("Preset Name", *parent), mainBox(get_vbox()) 
{
	labelText.set_text("Preset Name");
	mainBox->pack_start(labelText);
	mainBox->pack_start(textEntry);
	textEntry.set_text("Untitled");
	add_button("Cancel", Gtk::RESPONSE_CANCEL);
	add_button("Ok", Gtk::RESPONSE_OK);
	show_all();
}

std::string PresetDial::get_text() {

    return textEntry.get_text();

}
//class VoicePage/////////////////////////////////////////////////////
VoicePage::VoicePage(VoicesManager* AMannix,int group,Midi* AMidas,Gtk::Window* par)
{
	parent=par;
	Mannix=AMannix;	//voices mannager
	Midas=AMidas;
	voiceIndex=group;		//the group index
	nbParam=Mannix->returnGroupNumberParam(voiceIndex);	//how many params has this engine
	VoiceParams=Mannix->returnGroupParams(voiceIndex);	//we get its params
	ParamBox=new Gtk::VBox[nbParam];	
	ParamName=new Gtk::Label[nbParam];
	//each parameter is a scale or a list, but for the moment we don't know
	nbScales=0;
	nbLists=0;
	for(int i=0;i<nbParam;i++)
	{	
		if(VoiceParams[i].list==0)	//it's a Scale !
			nbScales++;
		else	//it's a List !
			nbLists++;
	}
	//Now we know so we can create all our widgets
	//let's go !
	paramList=new Gtk::ListBox[nbLists];
	paramListName=new Gtk::Label*[nbLists];
	paramScale=new Gtk::Scale**[nbScales];
	
	//top
		//preset box
	for(int i=0;i<128;i++)
	{
		string temp=to_string(i+1);
		temp+=" : "+Mannix->getPresetName(i,voiceIndex);
		presetCombo.append(temp);
	}
	presetCombo.set_active(0);	
	presetCombo.signal_changed().connect(sigc::mem_fun(*this, &VoicePage::load));
	presetBox.pack_start(presetCombo,Gtk::PACK_SHRINK);
	savePreset.set_image_from_icon_name ("document-save-symbolic");
	presetBox.pack_start(savePreset,Gtk::PACK_SHRINK,5);
	savePreset.signal_clicked().connect(sigc::mem_fun(*this, &VoicePage::save));
		//other top stuff
	Name=new Gtk::Label((Mannix->returnGroupName(voiceIndex)));
	GroupIndex=new Gtk::Label("Voice Group : "+std::to_string(voiceIndex+1));
	GroupAl= new Gtk::Alignment(0.8, 0.5, 0, 0);
	voidAl=new Gtk::Alignment(Gtk::ALIGN_START,Gtk::ALIGN_END,0,0);
	NameAl= new Gtk::Alignment(0.2, 0.5, 0, 0);

	GroupAl->add(*GroupIndex);
	NameAl->add(*Name);
	LabelBox.pack_start(*NameAl,Gtk::PACK_SHRINK,10);
	LabelBox.pack_start(*voidAl);
	LabelBox.pack_start(presetBox,Gtk::PACK_SHRINK,10);
	LabelBox.pack_end(*GroupAl);
	LabelBox.set_spacing(40);
	//params
	int indexScale=0;
	int indexList=0;
	for(int i=0;i<nbParam;i++)// for each param we're going to configure the scale or the list
	{
		ParamName[i].set_text(VoiceParams[i].name);	//the name of the parameter
		ParamBox[i].pack_start(ParamName[i],Gtk::PACK_SHRINK);
		if(VoiceParams[i].list==0)	//it's a scale
		{
			paramScale[indexScale]=new Gtk::Scale*;
			*paramScale[indexScale]=new Gtk::Scale(Gtk::Adjustment::create(VoiceParams[i].midiValue, 0, 128,1 , 0, 1),Gtk::ORIENTATION_VERTICAL);
			paramScale[indexScale][0]->set_digits(0);
			paramScale[indexScale][0]->set_inverted();	//not really inverted, it's just Gtk wich is inverted so it's just to go back to a normal orientation //stupid gtk
			paramScale[indexScale][0]->signal_value_changed().connect(sigc::bind<int, int>(sigc::mem_fun(*this, &VoicePage::slide),indexScale,i));	//so if the value of the scale is changed, it will call the slide function, with the index of the scale 
			ParamBox[i].pack_start(*paramScale[indexScale][0]);
			indexScale++;		
		}
		if(VoiceParams[i].list!=0)	//it's a list
		{
			paramListName[indexList]=new Gtk::Label[VoiceParams[i].list];
			for(int j=0;j<VoiceParams[i].list;j++)	//we need to get the name of each element of the list
			{
				paramListName[indexList][j].set_text(VoiceParams[i].listNames[j]);
				paramList[indexList].append(paramListName[indexList][j]);	
			}
			paramList[indexList].select_row(*paramList[indexList].get_row_at_index(VoiceParams[i].midiValue));
			paramList[indexList].signal_row_selected().connect(sigc::bind<int>(sigc::mem_fun(*this, &VoicePage::listChange),i));
			ParamBox[i].pack_start(paramList[indexList]);
			indexList++;
		}
		ScaleBox.pack_start(ParamBox[i]);
	}
	pack_start(LabelBox,Gtk::PACK_SHRINK,10);
	pack_start(ScaleBox);
	set_spacing(20);
	show_all();
}

VoicePage::~VoicePage()
{
	delete[] ParamName;
	delete GroupIndex;
	delete GroupAl;
	delete NameAl;
	delete voidAl;
	for(int i=0;i<nbLists;i++)
	{
		delete[] paramListName[i];
	}
	delete[] paramListName;
	delete[] paramList;	
	for(int i=0;i<nbScales;i++)
	{
		delete paramScale[i][0];
		delete[] paramScale[i];
	}
	delete[] paramScale;
	delete[] ParamBox;
	delete Name;
}


void VoicePage::slide(int scale,int param)
{
	int temp=paramScale[scale][0]->get_value();
	if(mapping==false && unmapping==false)
		Mannix->setGroupParameter(voiceIndex,param,temp);	//voices manager will send a message to all the voices concerned by this change
	else if(mapping==true)
	{
		Midas->armMapping(1,0,voiceIndex,param);
	}
	else if(unmapping==true)
		Midas->unMap(1,0,voiceIndex,param);
}

void VoicePage::listChange(Gtk::ListBoxRow* row,int param)
{
	int temp=row->get_index();
	Mannix->setGroupParameter(voiceIndex,param,temp);	//voices manager will send a message to all the voices concerned by this change
}

void VoicePage::refresh()
{
	int temp=0;
	int tempb=0;
	for(int i=0;i<nbParam;i++)
	{
		if(VoiceParams[i].list==0)
		{
			paramScale[temp][0]->set_value(VoiceParams[i].midiValue);
			temp++;
		}
		else if(VoiceParams[i].list>0)
		{
			paramList[tempb].select_row(*paramList[tempb].get_row_at_index(VoiceParams[i].midiValue));
			tempb++;
		}

	}
}

void VoicePage::load()
{
	
	Mannix->loadPreset(presetCombo.get_active_row_number(),voiceIndex);
}

void VoicePage::save()
{
	
	PresetDial pdialog(parent);
	int ans=pdialog.run();
	if(ans==Gtk::RESPONSE_OK)
	{
		Mannix->savePreset(presetCombo.get_active_row_number(),pdialog.get_text(),voiceIndex);
		int temp=presetCombo.get_active_row_number();
		
		string stemp=to_string(temp+1);
		stemp+=" : "+Mannix->getPresetName(temp,voiceIndex);
		presetCombo.remove_text(temp);
		presetCombo.insert(temp,stemp);
		presetCombo.set_active(temp);
		
		
	}
}
//class effectPage////////////////////////////////////////////////////

EffectPage::EffectPage(Mixer* Yann,int i,Midi* AMidas,Gtk::Window* par)	//basically the same thing as voicepage, so please refer to it
//differences : onOff button
//		interacts with mixer and not voices manager
{
	parent=par;
	Moix=Yann;
	Midas=AMidas;
	effectIndex=i;
	nbParam=Moix->returnEffectNumberParam(effectIndex);
	EffectParams=Moix->returnEffectParams(effectIndex);
	ParamBox=new Gtk::VBox[nbParam];
	ParamName=new Gtk::Label[nbParam];
	if(Moix->returnEffectActive(effectIndex))
	{
		OnOff.set_active(true);
		active=true;
	}
	else
	{
		OnOff.set_active(false);
		active=false;
	}
	OnOff.signal_state_flags_changed().connect(sigc::mem_fun(*this, &EffectPage::onOff)); 	//what to do if the on off button is activated

	//Really the same thing as voicespage
	nbScales=0;
	nbLists=0;
	for(int i=0;i<nbParam;i++)
	{	
		if(EffectParams[i].list==0)
			nbScales++;
		else
			nbLists++;
	}
	paramList=new Gtk::ListBox[nbLists];
	paramListName=new Gtk::Label*[nbLists];
	paramScale=new Gtk::Scale**[nbScales];
	Name=new Gtk::Label((Moix->returnEffectName(effectIndex)));
	EffectIndex=new Gtk::Label("Effect : "+std::to_string(effectIndex+1));
 	EffectAl= new Gtk::Alignment(0.8, 0.5, 0, 0);
	SwitchAl=new Gtk::Alignment(Gtk::ALIGN_START,Gtk::ALIGN_END,0,0);
	NameAl= new Gtk::Alignment(0.2, 0.5, 0, 0);
	EffectAl->add(*EffectIndex);
	NameAl->add(*Name);
	SwitchAl->add(OnOff);
	//preset box
	for(int i=0;i<128;i++)
	{
		string temp=to_string(i+1);
		temp+=" : "+Moix->getEffectPresetName(i,effectIndex);
		presetCombo.append(temp);
	}
	presetCombo.set_active(0);	
	presetCombo.signal_changed().connect(sigc::mem_fun(*this, &EffectPage::load));
	presetBox.pack_start(presetCombo,Gtk::PACK_SHRINK);
	savePreset.set_image_from_icon_name ("document-save-symbolic");
	presetBox.pack_start(savePreset,Gtk::PACK_SHRINK,5);
	savePreset.signal_clicked().connect(sigc::mem_fun(*this, &EffectPage::save));
	//
	LabelBox.pack_start(*NameAl,Gtk::PACK_SHRINK,10);
	LabelBox.pack_start(*SwitchAl);
	LabelBox.pack_start(presetBox,Gtk::PACK_SHRINK,10);
	LabelBox.pack_end(*EffectAl);
	LabelBox.set_spacing(40);
	int indexScale=0;
	int indexList=0;
	for(int i=0;i<nbParam;i++)
	{
		ParamName[i].set_text(EffectParams[i].name);
		ParamBox[i].pack_start(ParamName[i],Gtk::PACK_SHRINK);
		if(EffectParams[i].list==0)
		{
			paramScale[indexScale]=new Gtk::Scale*;
			*paramScale[indexScale]=new Gtk::Scale(Gtk::Adjustment::create(EffectParams[i].midiValue, 0, 128,1 , 0, 1),Gtk::ORIENTATION_VERTICAL);
			paramScale[indexScale][0]->set_digits(0);
			paramScale[indexScale][0]->set_inverted();
			paramScale[indexScale][0]->signal_value_changed().connect(sigc::bind<int, int>(sigc::mem_fun(*this, &EffectPage::slide),indexScale,i));
			ParamBox[i].pack_start(*paramScale[indexScale][0]);
			indexScale++;		
		}
		if(EffectParams[i].list!=0)
		{
			paramListName[indexList]=new Gtk::Label[EffectParams[i].list];
			for(int j=0;j<EffectParams[i].list;j++)
			{
				paramListName[indexList][j].set_text(EffectParams[i].listNames[j]);
				paramList[indexList].append(paramListName[indexList][j]);	
			}
			paramList[indexList].select_row(*paramList[indexList].get_row_at_index(EffectParams[i].midiValue));
			paramList[indexList].signal_row_selected().connect(sigc::bind<int>(sigc::mem_fun(*this, &EffectPage::listChange),i));
			ParamBox[i].pack_start(paramList[indexList]);
			indexList++;
		}
		ScaleBox.pack_start(ParamBox[i]);
	}
	pack_start(LabelBox,Gtk::PACK_SHRINK,10);
	pack_start(ScaleBox);
	set_spacing(20);
	show_all();
}

EffectPage::~EffectPage()
{
	delete[] ParamName;
	delete EffectIndex;
	delete EffectAl;
	delete NameAl;
	for(int i=0;i<nbLists;i++)
	{
		delete[] paramListName[i];
	}
	delete[] paramListName;
	delete[] paramList;
	
	for(int i=0;i<nbScales;i++)
	{
		delete paramScale[i][0];
		delete[] paramScale[i];
	}
	delete[] paramScale;
	delete[] ParamBox;
	delete Name;
}

void EffectPage::slide(int scale,int param)
{
	int temp=paramScale[scale][0]->get_value();
	if(mapping==false && unmapping==false)
		Moix->setEffectParameter(effectIndex,param,temp);	//mixer will send a message to the concerned effect
	else if(mapping==true)
		Midas->armMapping(0,0,effectIndex,param);
	else if(unmapping==true)
		Midas->unMap(0,0,effectIndex,param);
}

void EffectPage::listChange(Gtk::ListBoxRow* row,int param)
{
	int temp=row->get_index();
	Moix->setEffectParameter(effectIndex,param,temp);	//mixer will send a message to the concerned effect
}

void EffectPage::onOff(Gtk::StateFlags dontCare)
{
	if(OnOff.get_active ()!=active)
	{
		Moix->EffectToggle(effectIndex);
		active=!active;
	}
}
void EffectPage::refresh()
{
	int temp=0;
	int tempb=0;
	for(int i=0;i<nbParam;i++)
	{
		if(EffectParams[i].list==0)
		{
			paramScale[temp][0]->set_value(EffectParams[i].midiValue);
			temp++;
		}
		else if(EffectParams[i].list>0)
		{
			paramList[tempb].select_row(*paramList[tempb].get_row_at_index(EffectParams[i].midiValue));
			tempb++;
		}
	}

}

void EffectPage::load()
{
	
	Moix->loadEffectPreset(presetCombo.get_active_row_number(),effectIndex);

}

void EffectPage::save()
{
	
	PresetDial pdialog(parent);
	int ans=pdialog.run();
	if(ans==Gtk::RESPONSE_OK)
	{
		Moix->saveEffectPreset(presetCombo.get_active_row_number(),pdialog.get_text(),effectIndex);
		int temp=presetCombo.get_active_row_number();
		
		string stemp=to_string(temp+1);
		stemp+=" : "+Moix->getEffectPresetName(temp,effectIndex);
		presetCombo.remove_text(temp);
		presetCombo.insert(temp,stemp);
		presetCombo.set_active(temp);
		
		
	}
}
//class GlobalPage////////////////////////////////////////////////////
GlobalPage::GlobalPage(VoicesManager* AMannix,Mixer* AMoix,signalNewEngine* signal,signalEffectChange* sigC,signalUpMix* signalU,Midi* AMidas) :VolumeScale(Gtk::ORIENTATION_VERTICAL)
{
	//init 
	Midas=AMidas;
	signalAdd=signal;
	signalChange=sigC;
	signalUp=signalU;
	signalUp->connect(sigc::mem_fun(*this, &GlobalPage::onUp));
	Man=AMannix;
	Mi=AMoix;
	GroupInfo=Man->returnGroupInfo();
	nbEffects=Mi->returnNumberEffects();
	AddWin= new PopUpAdd(AMannix,signalAdd);

	//Voices Box	aka group mixer
	//it uses a flowbox, so we create the max number of channels (=number of voices)
	//and we merely filter the ones we want to show
	//not really elegant, but it's working well
	VoicesBox.set_min_children_per_line(NBVOICES);//we want a monoline flowbox
	VoicesBox.set_max_children_per_line(NBVOICES);	
	VoicesBox.set_selection_mode (Gtk::SELECTION_NONE);
	AddV.set_label("Add Engine");	//the button to open the pop up
	//all the elements of a channel
	MixerChannel=new Gtk::VBox[NBVOICES];
	ChannelLabel=new Gtk::Label[NBVOICES];
	ChannelVolume=new Gtk::Scale*[NBVOICES];
	ChannelMute=new Gtk::ToggleButton[NBVOICES];
	ChannelSolo=new Gtk::ToggleButton[NBVOICES];
	mutedbysolo=new bool[NBVOICES];	//muted by solo is used when unsolo a channel, we don't want to unmute all the other channels but only the ones muted by a solo
	for(int i=0;i<NBVOICES;i++)	//configuration / connection of each channel
	{
		mutedbysolo[i]=false;
		ChannelVolume[i] =new Gtk::Scale(Gtk::Adjustment::create(1, 0, 2,1 , 0, 1),Gtk::ORIENTATION_VERTICAL);
		ChannelLabel[i].set_text(Man->returnGroupName(i)+" "+std::to_string(i+1));
		ChannelVolume[i][0].set_digits(2);
		ChannelVolume[i][0].set_inverted();
		ChannelVolume[i][0].signal_value_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &GlobalPage::mix_slide),i));
		ChannelMute[i].set_label("m");
		ChannelMute[i].set_active(false);
		ChannelSolo[i].set_label("s");
		ChannelSolo[i].set_active(false);
		ChannelMute[i].signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(*this, &GlobalPage::onMute),i));
		ChannelSolo[i].signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(*this, &GlobalPage::onSolo),i));
		MixerChannel[i].pack_start(ChannelLabel[i],Gtk::PACK_SHRINK,20);
		MixerChannel[i].pack_start(ChannelVolume[i][0],true,true,10);
		MixerChannel[i].pack_start(ChannelSolo[i],Gtk::PACK_SHRINK);
		MixerChannel[i].pack_start(ChannelMute[i],Gtk::PACK_SHRINK);
		VoicesBox.insert(MixerChannel[i],i);
	}
	VoicesBox.set_filter_func(sigc::mem_fun(*this,&GlobalPage::on_filter));	//to filter the channels showed in the flowbox

	//Eff Box aka where we choose our effects
	
	EffectComb=new Gtk::ComboBoxText[nbEffects];
	ComboLabel=new Gtk::Label[nbEffects];
	SubBoxEff=new Gtk::VBox[nbEffects];
	for(int i=0;i<nbEffects;i++)	//we want to show all existing effects 
	{
		ComboLabel[i].set_text("Effect "+std::to_string(i+1));
		EffectComb[i].set_title("Effec "+std::to_string(i+1));
		for(int j=0;j<NB_EFFECT_ENGINES;j++)
		{
			EffectComb[i].append(returnEffectEngineName(j));
		}
		EffectComb[i].set_active(i);
		EffectComb[i].signal_changed().connect(sigc::bind<int>(sigc::mem_fun(*this, &GlobalPage::on_eff_change),i));
		SubBoxEff[i].pack_start(ComboLabel[i],Gtk::PACK_SHRINK);
		SubBoxEff[i].pack_start(EffectComb[i],Gtk::PACK_SHRINK);
		EffectBox.pack_start(SubBoxEff[i],Gtk::PACK_SHRINK,50);
	}

	//vol Box;

	VolumeLabel.set_text("Gain");
	VolumeScale.set_adjustment(Gtk::Adjustment::create(Mi->returnVolume(), 0, NBVOICES,1 , 0, 1));
	VolumeScale.set_digits(2);
	VolumeScale.set_inverted();			
	VolumeScale.signal_value_changed().connect(sigc::mem_fun(*this, &GlobalPage::slide));
	VolBox.pack_start(VolumeLabel,Gtk::PACK_SHRINK);
	VolBox.pack_start(VolumeScale);

	//Main Box Packing
	MainBox.pack_start(VoicesBox);
	MainBox.pack_start(AddV,Gtk::PACK_SHRINK,20);
	MainBox.pack_start(EffectBox,Gtk::PACK_SHRINK,20);
	MainBox.pack_end(VolBox,Gtk::PACK_SHRINK,20);
	pack_start(MainBox);
	AddV.signal_clicked().connect(sigc::mem_fun(*this, &GlobalPage::on_add_clicked));
	show_all();
}

bool GlobalPage::on_filter(Gtk::FlowBoxChild* child)
{
	bool btemp=false;
	int temp=child->get_index();
	if(GroupInfo[temp].active)	//we show only the actived groups
		btemp=true;
	return btemp;
}

void GlobalPage::onMute(int i)
{
	if(ChannelMute[i].get_active())	//we need to mute this channel
	{
		if(ChannelSolo[i].get_active())
			ChannelSolo[i].set_active(false);	//well we muted this channel ourselves
		for(int j=0;j<NBVOICES;j++)
		{
			if(i==Man->returnVoiceGroup(j))	//we check if the jvoice is in this muted group	
			{
				Mi->setVoiceVolume(j,0);	//mute
			}
		}
	}
	else	//unmute
	{
		for(int j=0;j<NBVOICES;j++)
		{
			if(i==Man->returnVoiceGroup(j))
			{
				Mi->setVoiceVolume(j,ChannelVolume[i][0].get_value());	//volume indicated by the slider
			}
		}
	}
		
}

void GlobalPage::onSolo(int i)
{
	if(ChannelSolo[i].get_active())	//solo on
	{

		if(ChannelMute[i].get_active())	 //Am I mute, can't be mute if I'm solo
		{
			ChannelMute[i].set_active(false);
			onMute(i);
		}

		for(int j=0;j<NBVOICES;j++)//if a group is not mute, we mute it
		{
			if(!ChannelSolo[j].get_active() && !ChannelMute[j].get_active())
			{
				mutedbysolo[j]=true;
				ChannelMute[j].set_active(true);
				onMute(j);
			}
		}
				
	}
	
	else	//unsolo
	{
		bool temp=false;
		for(int j=0;j<NBVOICES;j++)	//is there another solo on? yeah, I know... multiple solos...
		{
			if(ChannelSolo[j].get_active())
				temp=true;
		}
		if(temp==false)	//well we can unmute all the channels muted by a solo
		{
			for(int j=0;j<NBVOICES;j++)
			{
				if(mutedbysolo[j])
				{
					ChannelMute[j].set_active(false);
					onMute(j);
					mutedbysolo[j]=false;
				}
			}
		}
		else		//there is another solo, so I mute myself
		{
			ChannelMute[i].set_active(true);
			mutedbysolo[i]=true;
			onMute(i);
		}
	}
}
void GlobalPage::mix_slide(int i)
{
	//if(mapping==false && unmapping==false)	//later
	{
		if(!ChannelMute[i].get_active())
		{
			for(int j=0;j<NBVOICES;j++)
			{
				if(i==Man->returnVoiceGroup(j))//is the j voices concerned by this volume
				{
					Mi->setVoiceVolume(j,ChannelVolume[i][0].get_value());
				}
			}
		}
	}

}

GlobalPage::~GlobalPage()
{
	delete[] mutedbysolo;
	delete AddWin;
	delete[] EffectComb;
	delete[] ComboLabel;
	delete[] SubBoxEff;
	delete[] MixerChannel;
	delete[] ChannelLabel;
	for(int i=0;i<NBVOICES;i++)
		delete ChannelVolume[i];
	delete[] ChannelVolume;
	delete[] ChannelMute;
	delete[]ChannelSolo;
}
void GlobalPage::slide()
{
	if(mapping==false && unmapping==false)
	{
		float temp=VolumeScale.get_value();
		Mi->setVolume(temp);	//main volume
	}
	else if(mapping==true)
	{
		Midas->armMapping(0,2,-1,-1);
	}
	else if(unmapping==true)
	{
		Midas->unMap(0,2,-1,-1);
	}
}

void GlobalPage::on_add_clicked()
{
	AddWin->go();	//we show the pop up window
}

void GlobalPage::on_eff_change(int eff)
{
	int temp=EffectComb[eff].get_active_row_number(); //engine selected
	Mi->changeEffect(eff,temp);//mixer will handle the change
	signalChange->emit(eff,temp);//we signal the change
	
}
void GlobalPage::onUp(int gro)	//when we add or delete a group
{
	VoicesBox.set_filter_func(sigc::mem_fun(*this,&GlobalPage::on_filter)); //we refilter the channel mixer to reflect the change
	ChannelVolume[gro][0].set_value(Mi->returnVoiceVolume(gro));
	ChannelLabel[gro].set_text(Man->returnGroupName(gro)+" "+std::to_string(gro+1));
	if(!GroupInfo[gro].active)
	{
		Mi->setVoiceVolume(gro,1);
		ChannelSolo[gro].set_active(false);
		onSolo(gro);
		ChannelMute[gro].set_active(false);
	}
	else
	{
		for(int i=0;i<NBVOICES;i++)
		{
			if(ChannelSolo[i].get_active())
				ChannelMute[gro].set_active(true);
		}
	}
	
}
void GlobalPage::refresh()
{
	VolumeScale.set_value(Mi->returnVolume());
}
//class PopUpAdd//////////////////////////////////////////////////////

PopUpAdd::PopUpAdd(VoicesManager* AMannix,signalNewEngine* signal):Gtk::Window::Window (Gtk::WindowType::WINDOW_TOPLEVEL)
{

	signalAdd=signal;	//this signal is sent when the user click ok
	tempMannix=AMannix;
	resize(300, 250);
	freeVoices=tempMannix->returnNbFreeVoices();
	//Engine Box///
	ListEngine.set_title("Engine");
	LabelEng[0].set_text("Engine");
	LabelEng[1].set_text("NB Voices");
	for(int i=0;i<NB_ENGINES;i++)	//we want to show all the existing engines
	{
		ListEngine.append(returnEngineName(i));
	}
	ListEngine.set_active(0);

	SubBox[0].pack_start(LabelEng[0],Gtk::PACK_SHRINK,10);
	SubBox[0].pack_start(ListEngine,Gtk::PACK_SHRINK,10);

	SpinVoice.set_range(0,freeVoices);
	SpinVoice.set_digits(0);
	SpinVoice.set_increments(1,1);
	SpinVoice.set_numeric(true);
	SpinVoice.set_value(freeVoices);

	SubBox[1].pack_start(LabelEng[1],Gtk::PACK_SHRINK,10);
	SubBox[1].pack_start(SpinVoice,Gtk::PACK_SHRINK,10);
	
	EngineBox.pack_start(SubBox[0],Gtk::PACK_SHRINK,10);
	EngineBox.pack_end(SubBox[1],Gtk::PACK_SHRINK,10);
	
	//spin box///   for the range of notes
	Spin[0].set_range(0,127);//midi
	Spin[1].set_range(0,127);
	Spin[0].set_digits(0);//must be integer
	Spin[1].set_digits(0);
	Spin[0].set_increments(1,10);
	Spin[1].set_increments(1,10);
	Spin[0].set_numeric(true);
	Spin[1].set_numeric(true);
	Spin[0].set_value(0);
	Spin[1].set_value(127);
	Spin[0].set_can_focus(true);
	Spin[0].signal_value_changed().connect(sigc::mem_fun(*this,&PopUpAdd::on_spin_change));
	Spin[1].signal_value_changed().connect(sigc::mem_fun(*this,&PopUpAdd::on_spin_change));
	LabelSpin[0].set_text("Note Min");
	LabelSpin[1].set_text("Note Max");
	for(int i=0;i<2;i++)
	{
		LSBox[i].pack_start(LabelSpin[i],Gtk::PACK_SHRINK,10);
		LSBox[i].pack_end(Spin[i],Gtk::PACK_SHRINK,10);
	}
	SpinBox.pack_start(LSBox[0],Gtk::PACK_SHRINK,10);
	SpinBox.pack_end(LSBox[1],Gtk::PACK_SHRINK,10);

	//bottom box///
	OkButton.set_label("Ok");
	CancelButton.set_label("Cancel");
	OkButton.signal_clicked().connect(sigc::mem_fun(*this,&PopUpAdd::on_ok_clicked));
	CancelButton.signal_clicked().connect(sigc::mem_fun(*this, &PopUpAdd::on_cancel_clicked));	
	BottomBox.pack_start(CancelButton,Gtk::PACK_SHRINK,10);
	BottomBox.pack_end(OkButton,Gtk::PACK_SHRINK,10);

	//final///
	MainBox.pack_start(SpinBox,Gtk::PACK_SHRINK,10);
	MainBox.pack_start(EngineBox,Gtk::PACK_SHRINK,10);
	MainBox.pack_end(BottomBox,Gtk::PACK_SHRINK,10);
	add(MainBox);
}
void PopUpAdd::on_cancel_clicked()	//I know, I really don't like to hide things...
{
	hide();
}

void PopUpAdd::on_ok_clicked()	
{
	hide();
	set_modal(false);
	int tempmin=Spin[0].get_value();
	int tempmax=Spin[1].get_value();
	int tempnbvoice=SpinVoice.get_value();
	int tempeng=ListEngine.get_active_row_number();
	signalAdd->emit(tempnbvoice,tempeng,tempmin,tempmax);	//Gui will handle this and add or delete a group if nbvoices>0
	 
}

void  PopUpAdd::go()	//when the pop up is opened
{
	set_position(Gtk::WIN_POS_MOUSE); 
	freeVoices=tempMannix->returnNbFreeVoices();
	SpinVoice.set_range(0,freeVoices);
	show_all();
	set_modal(true);	//we can't use the other window while the pop up is active
}

void PopUpAdd::on_spin_change()	//when the lowest or the highest note is changed we need to modify the range of the other one to avoid min>max
{
	int tempmin=Spin[0].get_value();
	int tempmax=Spin[1].get_value();
	if(tempmin>tempmax)
		Spin[1].set_value(tempmin);
}

PopUpAdd::~PopUpAdd()
{
	;	//new-less
}
	
//class tuningSettings : public tuningSettings////////////////////////
TuningSettings::TuningSettings(Gtk::Window* parent,int key,float freq) : Gtk::Dialog("Tuning Settings (for .scl only)",*parent), mainBox(get_vbox())
{
	mainBox->pack_start(SpinBox, Gtk::PACK_SHRINK);
	for(int i=0;i<2;i++)
	{
		SpinBox.pack_start(LSBox[i],Gtk::PACK_SHRINK,20);
		LSBox[i].pack_start(LabelSpin[i],Gtk::PACK_SHRINK,10);
		LSBox[i].pack_start(Spin[i],Gtk::PACK_SHRINK,20);
	}
	Spin[0].set_range(0,127);//midi
	Spin[1].set_range(20,SAMPLE_RATE/2); //Hz
	Spin[0].set_digits(0);//must be integer
	Spin[1].set_digits(4);	
	Spin[0].set_increments(1,10);
	Spin[1].set_increments(1,10);
	Spin[0].set_numeric(true);
	Spin[1].set_numeric(true);
	Spin[0].set_value(key);
	Spin[1].set_value(freq);
	Spin[0].set_can_focus(true);
	Spin[1].set_can_focus(true);
	LabelSpin[0].set_text("Reference Key");
	LabelSpin[1].set_text("Frequency");
  	add_button("Cancel", Gtk::RESPONSE_CANCEL);
	add_button("Ok", Gtk::RESPONSE_OK);
	show_all();
}

int TuningSettings::getKey()
{
	return Spin[0].get_value();
}

float TuningSettings::getFreq()
{
	return Spin[1].get_value();
}

//class DeviceSettings////////////////////////////////////////////////
DeviceSettings::DeviceSettings(Gtk::Window* parent,Midi* DMidas) : Gtk::Dialog("Midi Settings",*parent), dialog(get_vbox())
{
	//mainBox->set_orientation(Gtk::ORIENTATION_HORIZONTAL);
	dialog->pack_start(mainBox, Gtk::PACK_SHRINK,10);
	mainBox.pack_start(midiBox, Gtk::PACK_SHRINK,10);
	DMidas->refreshDevicesList();
	nbDevices=DMidas->returnNbDevices();
	devices=DMidas->returnDevices();
	//int nbIn=0;
	int inSel=DMidas->returnInDevice();
	int inComboSel=0;
	int nbIn=0;
	for(int i=0;i<nbDevices;i++)	//we need to know how many input we have
	{
		//cout<<endl<<devices[i].name<<endl;
		if(devices[i].input)
		{
			DevicesList.append(devices[i].interface+" : "+devices[i].name);
			if(devices[i].id==inSel)
				inComboSel=nbIn;
			nbIn++;
			
		}
	}
	DevicesList.set_active(inComboSel);

	ComboName.set_text("Midi Input");
	midiBox.pack_start(ComboName, Gtk::PACK_SHRINK);
	midiBox.pack_start(DevicesList, Gtk::PACK_SHRINK,10);
  	add_button("Cancel", Gtk::RESPONSE_CANCEL);
	add_button("Ok", Gtk::RESPONSE_OK);
	show_all();
}

int DeviceSettings::getId()
{
	int temp=DevicesList.get_active_row_number();
	int tempb=0;
	for(int i=0;i<nbDevices;i++)
	{
		if(devices[i].input)
		{
			if(tempb==temp)
				return i;
			tempb++;
		}
	}
	return 0;
}
//class ChannelSelection//////////////////////////////////////////////
ChannelSelection::ChannelSelection(Gtk::Window* parent,Midi* DMidas) : Gtk::Dialog("Channel Selection",*parent), dialog(get_vbox()) 
{
	dialog->pack_start(mainBox, Gtk::PACK_SHRINK,10);
	mainBox.pack_start(spinBox, true,true,10);
	spin.set_range(1,16);//midi channel
	spin.set_digits(0);//must be integer
	spin.set_increments(1,1);
	spin.set_numeric(true);
	spin.set_value(DMidas->returnChannel()+1);
	spin.set_can_focus(true);
	spinLabel.set_text("Input Channel");
	spinBox.pack_start(spinLabel,Gtk::PACK_SHRINK);
	spinBox.pack_start(spin,Gtk::PACK_SHRINK,10);
	add_button("Cancel", Gtk::RESPONSE_CANCEL);
	add_button("Ok", Gtk::RESPONSE_OK);
	show_all();
}
//class autogui///////////////////////////////////////////////////////
Gui::Gui(VoicesManager* AMannix,Mixer* AMoix,Midi* AMidas,std::string awhereami) 
{
	whereami=awhereami;
	string templogo(DATA_PATH);
	templogo+="/logo/";
	templogo+="bereznnalog.png";
	set_icon_from_file(templogo.c_str());
	for(int i=0;i<12;i++)
		pressed[i]=false;
	set_title("[Be][Re][Zn][Na]");
	tempMannix=AMannix;
	tempMoix=AMoix;
	Midas=AMidas;
	add(MainBoxV);
	nbEffects=tempMoix->returnNumberEffects();
	
	small=Gtk::IconSize::register_new("small",16,16);
	record.set_from_icon_name ("media-record-symbolic", small);
	//Menu
	tada.add(record);
	tada.set_transition_duration(1000);
	tada.show_all();
	tada.set_reveal_child(false);
	tada.set_transition_type(Gtk::REVEALER_TRANSITION_TYPE_CROSSFADE);

	MainBoxV.pack_start(Menu,Gtk::PACK_SHRINK);
	TuningEdit.set_label("Tuning");
	//Midim.set_label("Midi");
	Midim.add(MidiBox);
	MidiBox.pack_start(MidiLabel,Gtk::PACK_SHRINK);
	MidiLabel.set_text("Midi  ");
	MidiBox.pack_start(tada,Gtk::PACK_SHRINK);
	
	Menu.append(TuningEdit);
	Menu.append(Midim);
	TuningEdit.set_submenu(TuningMenu);
	Midim.set_submenu(MidiMenu);
	Tuning.set_label("Load Scale");
	Tuning.signal_activate().connect(sigc::mem_fun(*this,&Gui::tuning));
	refTuning.set_label("Tuning Settings");
	refTuning.signal_activate().connect(sigc::mem_fun(*this,&Gui::tuningSettings));
	TuningMenu.append(Tuning);
	TuningMenu.append(refTuning);
	selDevice.set_label("Device");
	selDevice.signal_activate().connect(sigc::mem_fun(*this,&Gui::deviceSelection));
	MidiMenu.append(selDevice);
	chanSel.set_label("Channel");
	chanSel.signal_activate().connect(sigc::mem_fun(*this,&Gui::channelSel));
	MidiMenu.append(chanSel);
	mappingOn.set_label("Arm CC-Mapping");
	mappingOn.signal_activate().connect(sigc::mem_fun(*this,&Gui::toggleMapping));
	MidiMenu.append(mappingOn);
	unmappingOn.set_label("Arm CC-UnMapping");
	unmappingOn.signal_activate().connect(sigc::mem_fun(*this,&Gui::toggleUnmapping));
	MidiMenu.append(unmappingOn);
	unmapAll.set_label("UnMap All CC");
	unmapAll.signal_activate().connect(sigc::mem_fun(*this,&Gui::unmapAllClicked));
	MidiMenu.append(unmapAll);
	//Voice label tab//////////

	PageLabel=new Gtk::Label[NBVOICES];
	PageTab=new Gtk::HBox[NBVOICES];
	DeletePage=new Gtk::ToolButton[NBVOICES];
	DeleteBox=new Gtk::VBox[NBVOICES];
	for(int i=0;i<NBVOICES;i++)	//we create the max number of page tab, and we're going to use only the ones active
	{
		PageLabel[i].set_text(tempMannix->returnGroupName(0)+" 1");
		PageTab[i].pack_start(PageLabel[i],Gtk::PACK_SHRINK,20);
		DeletePage[i].set_icon_name("window-close-symbolic");
		DeletePage[i].show_all();
		DeleteBox[i].pack_start(DeletePage[i],false,false,0);
		PageTab[i].pack_end(DeleteBox[i],false,false,0);
		PageTab[i].show_all();
		DeletePage[i].signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(*this, &Gui::on_delete_page_clicked),i));
	}
	Bookie.set_scrollable();
	Voice=new VoicePage*[NBVOICES];
	Voice[0]=new VoicePage(AMannix,0,Midas,this);
	for(int i=1;i<NBVOICES;i++)	//we create void voicespage for convenience
		Voice[i]=new VoicePage(AMannix,-1,Midas,this);
	//Effect Page
	Effect=new EffectPage*[nbEffects];
	for(int i=0;i<nbEffects;i++)
		Effect[i]=new EffectPage(AMoix,i,Midas,this);
	// Globals Page
	Glob=new GlobalPage(AMannix,AMoix,&signalAdd,&signalChange,&signalUp,Midas);
	Bookie.append_page(*Glob,"Globals");
	Bookie.set_tab_reorderable(*Glob);
	Bookie.append_page(*Voice[0],PageTab[0]);
	Bookie.set_tab_reorderable(*Voice[0]);		
	nbVoicePages=1;
	for(int i=0;i<nbEffects;i++)
	{
		Bookie.append_page(*Effect[i],tempMoix->returnEffectName(i)+" "+std::to_string(i+1));
		Bookie.set_tab_reorderable(*Effect[i]);
	}
	MainBoxV.pack_start(Bookie);

	//foot info labels
	std::string temp(STAGEVERSION);
	info[0].set_text("[Be][Re][Zn][Na]");
	info[1].set_text("v"+std::to_string(MAJORVERSION)+"."+std::to_string(MINORVERSION)+"."+std::to_string(REVVERSION)+temp);
	info[2].set_text(std::to_string(NBVOICES)+" Voices");
	info[3].set_text("Sample Rate : "+std::to_string(SAMPLE_RATE)+" Hz");
	info[4].set_text("Buffer Size : "+std::to_string(BUFF));
	for(int i=0;i<4;i++)
		FootLabelBox.pack_start(info[i]);

	//FootLabelBox.pack_start(tada);
	MainBoxV.pack_start(FootLabelBox,Gtk::PACK_SHRINK);


	//various signals
	this->signal_key_press_event().connect(sigc::mem_fun(*this, &Gui::onKeyPress), false);
	this->signal_key_release_event().connect(sigc::mem_fun(*this, &Gui::onKeyRelease), false);
	signalChange.connect(sigc::mem_fun(*this,&Gui::changeEffect));
	signalAdd.connect(sigc::mem_fun(*this, &Gui::addVoice));
	Glib::signal_timeout().connect( sigc::mem_fun(*this, &Gui::refreshValue), 10);
	
	show_all();
	int width;
	int height;
	int bin;
	get_preferred_width(bin,width);
	get_preferred_height(bin,height);
	if(width<640)
		width=640;
	if(height<480)
		height=480;
	resize(width,height);
}

void Gui::changeEffect(int eff,int eng)
{
	Bookie.remove_page(*Effect[eff]);	//we need to change the effect page
	delete Effect[eff];	//so we simply delete it
	Midas->unMapSetP(0,eff);
	Effect[eff]=new EffectPage(tempMoix,eff,Midas,this);	//and create a new one
	Bookie.insert_page(*Effect[eff],tempMoix->returnEffectName(eff)+" "+std::to_string(eff+1),1+nbVoicePages+eff);
}
void Gui::addVoice(int nbvoice,int eng,int min,int max)
{
	if(nbvoice>0)
	{
		int gro=0;
		GroupData* tempgroupinfo=tempMannix->returnGroupInfo();
		for(int i=0;i<NBVOICES;i++)
		{
			if(tempgroupinfo[i].active==false)
			{
				gro=i;
				break;
			}	
		}
		tempMannix->addGroup(gro,nbvoice,eng,min,max);
		delete Voice[gro];
		Voice[gro]=new VoicePage(tempMannix,gro,Midas,this);
		PageLabel[gro].set_text(tempMannix->returnGroupName(gro)+std::to_string(1+gro));
		Bookie.insert_page(*Voice[gro],PageTab[gro],nbVoicePages+1);
		Bookie.set_tab_reorderable(*Voice[gro]);
		nbVoicePages++;
		signalUp.emit(gro);
	}
}

void Gui::on_delete_page_clicked(int gro)
{
	tempMannix->deleteGroup(gro);
	Midas->unMapSetP(1,gro);
	Bookie.remove_page(*Voice[gro]);
	nbVoicePages--;
	signalUp.emit(gro);
}
bool Gui::onKeyPress(GdkEventKey* event)//simple virtual keyboard, note on, voices manager will send excite message to each concerned voices
{
	 //std::cout << event->keyval << ' ' << event->hardware_keycode << ' ' << event->state << std::endl;
	int temp=event->hardware_keycode;			
	if( temp==52 && pressed[0]==false)
	{
		tempMannix->noteOn((int)53,(float)1);
		pressed[0]=true;
	}
	else if( temp==39 && pressed[1]==false)
	{
		tempMannix->noteOn((int)54,(float)1);
		pressed[1]=true;
	}
	else if( temp==53 && pressed[2]==false)
	{
		tempMannix->noteOn((int)55,(float)1);
		pressed[2]=true;
	}
	else if( temp==40 && pressed[3]==false)
	{
		tempMannix->noteOn((int)56,(float)1);
		pressed[3]=true;
	}
	else if( temp==54 && pressed[4]==false)
	{
		tempMannix->noteOn((int)57,(float)1);
		pressed[4]=true;
	}
	else if( temp==41 && pressed[5]==false)
	{
		tempMannix->noteOn((int)58,(float)1);
		pressed[5]=true;
	}
	else if( temp==55 && pressed[6]==false)
	{
		tempMannix->noteOn((int)59,(float)1);
		pressed[6]=true;
	}
	else if( temp==56 && pressed[7]==false)
	{
		tempMannix->noteOn((int)60,(float)1);
		pressed[7]=true;
	}
	else if( temp==43 && pressed[8]==false)
	{
		tempMannix->noteOn((int)61,(float)1);
		pressed[8]=true;
	}
	else if( temp==57 && pressed[9]==false)
	{
		tempMannix->noteOn((int)62,(float)1);
		pressed[9]=true;
	}
	else if( temp==44 && pressed[10]==false)
	{
		tempMannix->noteOn((int)63,(float)1);
		pressed[10]=true;
	}
	else if( temp==58 && pressed[11]==false)
	{
		tempMannix->noteOn((int)64,(float)1);	
		pressed[11]=true;
	}

	return false;
}

bool Gui::onKeyRelease(GdkEventKey* event)//simple virtual keyboard, note off, voices manager will send end message to each concerned voices
{
	 //std::cout << event->keyval << ' ' << event->hardware_keycode << ' ' << event->state << std::endl;
	int temp=event->hardware_keycode;			
	if( temp==52 && pressed[0]==true)
	{
		tempMannix->noteOff((int)53);
		pressed[0]=false;
	}
	else if( temp==39 && pressed[1]==true)
	{
		tempMannix->noteOff((int)54);
		pressed[1]=false;
	}
	else if( temp==53 && pressed[2]==true)
	{
		tempMannix->noteOff((int)55);
		pressed[2]=false;
	}
	else if( temp==40 && pressed[3]==true)
	{
		tempMannix->noteOff((int)56);
		pressed[3]=false;
	}
	else if( temp==54 && pressed[4]==true)
	{
		tempMannix->noteOff((int)57);
		pressed[4]=false;
	}
	else if( temp==41 && pressed[5]==true)
	{
		tempMannix->noteOff((int)58);
		pressed[5]=false;
	}
	else if( temp==55 && pressed[6]==true)
	{
		tempMannix->noteOff((int)59);
		pressed[6]=false;
	}
	else if( temp==56 && pressed[7]==true)
	{
		tempMannix->noteOff((int)60);
		pressed[7]=false;
	}
	else if( temp==43 && pressed[8]==true)
	{
		tempMannix->noteOff((int)61);
		pressed[8]=false;
	}
	else if( temp==57 && pressed[9]==true)
	{
		tempMannix->noteOff((int)62);
		pressed[9]=false;
	}
	else if( temp==44 && pressed[10]==true)
	{
		tempMannix->noteOff((int)63);
		pressed[10]=false;
	}
	else if( temp==58 && pressed[11]==true)
	{
		tempMannix->noteOff((int)64);	
		pressed[11]=false;
	}

	return false;
}

void Gui::tuning()	//to set the sc of BeReZnNa
{
	Gtk::FileChooserDialog tuningDialog(*this, "Select a Scale", Gtk::FILE_CHOOSER_ACTION_OPEN);
	tuningDialog.set_current_folder("./Scales");
	tuningDialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
	tuningDialog.add_button("Open", Gtk::RESPONSE_OK);
	Glib::RefPtr<Gtk::FileFilter> noodleFilter = Gtk::FileFilter::create();
	noodleFilter->set_name("Noodle files");
	noodleFilter->add_pattern("*.noodle");
	Glib::RefPtr<Gtk::FileFilter> sclFilter = Gtk::FileFilter::create();
	sclFilter->set_name("Scala files");
	sclFilter->add_pattern("*.scl");
	tuningDialog.add_filter(sclFilter);
	tuningDialog.add_filter(noodleFilter);
	tuningDialog.set_resizable(true);
 	int res = tuningDialog.run();
  	if(res == Gtk::RESPONSE_OK) 
	{
		if(tuningDialog.get_filter()==noodleFilter)
		{
			std::string fileName = tuningDialog.get_filename();
    			tempMannix->changeScale(fileName);	
		}
		else if(tuningDialog.get_filter()==sclFilter)	//scl file, we need do know the ref key and ref freq, but for now I will simply choose 69 440
		{
			std::string fileName = tuningDialog.get_filename();
			tempMannix->loadScl(fileName);
		}
   	}	

}

void Gui::tuningSettings()
{
	TuningSettings tdialog(this,tempMannix->returnRef(),tempMannix->returnFreq());
	int ans=tdialog.run();
	if(ans==Gtk::RESPONSE_OK)
	{
		tempMannix->setRef(tdialog.getKey());
		tempMannix->setFreq(tdialog.getFreq());
		tempMannix->updatePitch();
	}
}

void Gui::deviceSelection()
{
	DeviceSettings mdialog(this,Midas);
	int ans=mdialog.run();
	if(ans==Gtk::RESPONSE_OK)
	{
		Midas->changeDevice(mdialog.getId());
	}
}

void Gui::channelSel()
{
	ChannelSelection cdialog(this,Midas);
	int ans=cdialog.run();
	if(ans==Gtk::RESPONSE_OK)
	{
		Midas->setChannel(cdialog.getChannel());
	}
}

void Gui::toggleMapping()
{
	Merlin.disconnect();
	mapping=!mapping;
	if(mapping==false)
	{
		mappingOn.set_label("Arm CC-Mapping");
		Midas->unArm();
		tada.set_reveal_child(false);
	}
	else
	{
		mappingOn.set_label("CC-Mapping Armed");
		record.set_from_icon_name ("media-record-symbolic", small);
		Merlin=Glib::signal_timeout().connect( sigc::mem_fun(*this, &Gui::abracadabra), 1000);
		if(unmapping)
		{
			unmapping=false;
			unmappingOn.set_label("Arm CC-UnMapping");
		}	
	}
}

void Gui::toggleUnmapping()
{
	Merlin.disconnect();
	unmapping=!unmapping;
	if(unmapping==false)
	{
		unmappingOn.set_label("Arm CC-UnMapping");
		tada.set_reveal_child(false);
	}
	else
	{
		unmappingOn.set_label("CC-UnMapping Armed");
		record.set_from_icon_name ("list-remove-symbolic", small);
		Merlin=Glib::signal_timeout().connect( sigc::mem_fun(*this, &Gui::abracadabra), 1000);
		if(mapping)
		{
			mapping=false;
			Midas->unArm();
			mappingOn.set_label("Arm CC-Mapping");
		}
	}
}

void Gui::unmapAllClicked()
{
	if(mapping)
	{
		mapping=false;
		Midas->unArm();
		mappingOn.set_label("Arm CC-Mapping");
	}
	if(unmapping)
	{
		unmapping=false;
		unmappingOn.set_label("Arm CC-UnMapping");
		tada.set_reveal_child(false);
		Merlin.disconnect();
	}

	Midas->unMapAll();
}
bool Gui::refreshValue()
{

		GroupData* tempgroupinfo=tempMannix->returnGroupInfo();
	for(int i=0;i<NBVOICES;i++)
	{
		if(tempgroupinfo[i].active==TRUE)
		Voice[i]->refresh();
	}
	for(int i=0;i<nbEffects;i++)
		Effect[i]->refresh();
	Glob->refresh();
	return true;
}
bool Gui::abracadabra()
{
	if(mapping or unmapping)
	{
		bool temp=tada.get_reveal_child();
		tada.set_reveal_child(!temp);
	}
	else 
		tada.set_reveal_child(false);
	return true;
}
Gui::~Gui()
{
	delete Glob;
	for(int i=0;i<NBVOICES;i++)
	{
			delete Voice[i];
	}
	for(int i=0;i<nbEffects;i++)
		delete Effect[i];
	delete[] Voice;
	delete[] Effect;
	delete[] PageTab;
	delete[] PageLabel;
	delete[] DeletePage;
	delete[] DeleteBox;
}
