/*BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _EFFECT_HPP_
#define _EFFECT_HPP_

#include "main.hpp"

/*
	virtual effect class
	the mother of all effects
	to write a new effect please overwrite all virtual functions=0
*/
using namespace std;

class Effect
{
	protected :
	string name;
	bool active;

	param* params;
	int numberParams;
	public :
	Effect(string Ename,int nbparam);
	bool on(){active=!active;return active;};
	bool isOn(){return active;};

	int returnNumberParams(){return numberParams;};
	string returnName(){return name;};
	param* returnParams(){return params;};
	virtual~Effect();
	virtual void run(float* buffy,int bufferSize)=0;//what to do when you have a buffer to process
	virtual void modifyParameter(int param, int value)=0;// add the value to one parameter, well not exactly, the voice decides what it wants to do with this 8bit integer; legacy
	virtual void setParameter(int param, int value)=0;//set a parameter, value is a 8bit integer, the voice can use directly this value or another, as you want please refer to any effect for more informations
	void loadPreset(int preset); // load a specified preset
	void savePreset(int preset,string presetName);// save the current voice settings to the specified preset
	void createBank();
	string getPresetName(int preset);
};

#endif

