/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef _FIRFILTER_HPP_
#define _FIRFILTER_HPP_
/*
	A Finite Impulse Filter
	Only LP for the moment
	but easily adaptable to be a multimode filter
*/
#include "main.hpp"
#include "utils.hpp"
#include "effect.hpp"
using namespace std;

typedef float* (*jump)(int);
class FIRFilter : public Effect
//lp low pass FIR filter, for high or bandpass simple combinations of FIR lp and ap can be achieved
{
	private :
	float* lbuffer; //1 sample delays
	float* rbuffer;
	float* sincWeights;
	float* windowWeights;
	float* weights;
	jump windowJump[6];	//well just for the fun, ah ah..
	int window;	//what kind of function is used for the windowing
			//0-rect 1-bartlett 2-hanning 3-hamming 4-blackman
	float cutoff;	//cutoff
	int length;	//number of weights
	int order;
	int index;	//don't know, just a feeling, I think it can be useful
	public :
	FIRFilter();
	void computeWeights();
	void windowing();
	virtual void run(float* input, int inputSize)override;//callback what to do with buffer to process
	virtual void modifyParameter(int param, int value)override;// add the value to one parameter
	virtual void setParameter(int param, int value)override;//set the value of one parameter
	~FIRFilter();
};


#endif
