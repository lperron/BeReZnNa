/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// Cups Of Tea : 164
//Code-Gazing : 0
//Mind-Blowns : 2
//Segmentation Faults : 124
//Meaningless Bugs : 4
//Meaningless Bugs Actually Not Meaningless : 3
//
//Journal :
//
// -First Line of Code Sun Jan 21, 13h08
// -First Compilation Attempted Sun Jan 21, 20h27
// -First Compilation Completed Mon Jan 22, 12h59
// -First Segmentation Fault Mon Jan 22, 12h59
// -Segmentation Faults>Cups of Tea Mon Jan 22, 15h22
// -First Cry Tue Jan 23, 13h40
// -First Note (@440Hz) Tue Jan 23, 20h44
// -AZEFHAZOUFHSIIDFHAOZEFOBBCIUIHUVIOUHUOFHAIZEEAFZIHOUPHIZFUEPIHRZUAUHOAIHFY
// -...
// -First Polyphonic Harpsichord Thr Jan 25, 17:07
// -First Effect Thr Jan 25, 22:30
// -First Preset Saved Fri Jan 26, 14h05
// -First Preset Loaded Fri Jan 26, 14h21
// -"DIE GTK+ DIE !!!" Mon Jan 29, 17h00
// -"Viva Gtkmm !!!" Tue Jan 30, 22h17
// -Segmentation Faults<Cups of Tea, with the 42th cup. "BACK IN DA GAME BIATCH!!!" Tue Jan 30, 22h29
// -First voice controlled by AutoGui Wen Jan 31, 17h32
// -First Split/Layer Fri Feb 2,21h44
// -First Voice added by AutoGui Sun Feb 4, 11h41
// -"Autogui Rocks!" Sun Feb 4, 22h36
// -AutoGui Can Manage Effects and Voices Now Mon Feb 5, 17h12
// -Mixer and VoicesManager are now independants from childs of Effects and Voices Mon Feb 5, 17h30
// -BeReZnNa = 1.0MB Mon Feb 5,21h28  
// -AutoGui Has A Mixer Now, with Channel Volume, mute and solo Tue Feb 6, 11h07
// -Version 0.2a Tue Feb 6, 15h46
// -Theads Sync CPU-friendly Wed Feb 7, 18h25
// -100th Cup Of Tea Fri Feb 9, 11h15
// -Now it's comments time, aka the "I hate myself so much" phase, Tue Feb 13, 23h07
// -First Scale Loaded : La Monte Young's Well-Tuned, Thu Feb 15, 18h29 
// -Scale Management by AutoGui, Thu Feb, 21h58
// -First .scl Scale Loaded : Richard D. James's Monologue 1, Fri Feb 16, 19h01
// -.scl Can Be Loaded By AutoGui, Sun Feb 18, 22h07
// -First Midi Communication, Tue Feb 20, 21h22
// -Midi Callback CPU-friendly, Tue Feb 20, 22h15
// -100th Segmentation Fault, Thank You PortMidi, Wed Feb 21, 15h45
// -First CC mapping, Sat Feb 24, 16h11
// -First Voice Preset Managed By Autogui, Sat Mar 17, 15h09
// -First Effect Preset Managed By AutoGui, Sat Mar 17, 15h25

#include "main.hpp"
#include "utils.hpp"
#include "voice.hpp"
#include "voicesmanager.hpp"
#include "mixer.hpp"
#include "midi.hpp"
#include "autogui.hpp"

/*typedef struct
{
	Mixer* Mixx;
	bool goOn;
}ThreadData;*/
bool goOn=false;


void *mixerT(void * Mixx) //this thread handles 2nd order callback, which is sync with 1st order callback  (1st order is pa callback)
{
	//ThreadData* stuff=(ThreadData*) data;
	Mixer* Maxine=(Mixer*) Mixx;
	while(goOn==true)
	{
			//pthread_testcancel();
			Maxine->mix();			
	}	
	Maxine->stopPa();
	goOn=true;
	return NULL;
}



using namespace std;



int main(int argc, char *argv[])	//main thread, for object creation / ui
{
	{
		//cout<<endl<<"data path :"<<(DATA_PATH)<<endl;
		string whereami=argv[0];
		std::mutex mutexVar; //mutex for thread sync, there's also a condition variable between pa callback and mixer::mix for cpu-friendliness

		srand (time(NULL));
		Voice **Engine=new Voice*[NBVOICES]; // An array of all our voices (sound engines, like karplus or additive)
		VoicesManager Manie(NBVOICES,&Engine, &mutexVar);	//Voices manager transmits informations to our voices, like parameter changes, note on, etc... according to the group of each voice : a group is a set of similar voices, one example can be a first group of 4 Karplus Strong, a second of 4 Additive and a third of 8 Karplus Strong, each group has its own parameters, reacts to a certain interval of notes, etc...
		pthread_t MixerThread;
		{
		Mixer Maxx(NBVOICES,1,BUFF,&Engine, &mutexVar); //Mixer handles callbacks, it asks data to each voices, and feeds effect engines 
		//ThreadData* data;
		//data=new ThreadData;
		//data->Mixx=&Maxx;
		//data->goOn=true;
		goOn=true;
		pthread_create(&MixerThread, NULL, mixerT, &Maxx);
		Midi Mid(&Manie,&Maxx);
    		Gtk::Main app(argc, argv); //for the gui
 		Gui Roux(&Manie,&Maxx,&Mid,whereami); //Gui
		Gtk::Main::run(Roux); //Gui main loop
	   	/*	while(1)	//if you like terminal...
			{
				simple_keyboard(&Manie);	
			}*/	
		//pthread_detach(MixerThread);
		//pthread_cancel(MixerThread);
		goOn=false;
		pthread_join(MixerThread,NULL);
		}	

					
	}
	return EXIT_SUCCESS;
}
