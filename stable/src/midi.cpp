/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "midi.hpp"
#include "voicesmanager.hpp"
#include "mixer.hpp"

using namespace std;

void callback(PtTimestamp timestamp,void *userData)
{
	pmData *midiInfo = (pmData*)userData;
	
	int status=Pm_Poll(midiInfo->midas);
	if(status)//we have data:!!!
	{
		int length=Pm_Read(midiInfo->midas,midiInfo->buffer,1);
		if(length>0)
		{
			//cout<<endl<<(long) Pm_MessageStatus(midiInfo->buffer[0].message)<<endl<<(long) Pm_MessageData1(midiInfo->buffer[0].message)<<endl<<(long) Pm_MessageData2(midiInfo->buffer[0].message)<<endl<<endl;
			if((long)Pm_MessageStatus(midiInfo->buffer[0].message)==(0x90+midiInfo->channel))	//note on
			{
				midiInfo->Mannie->noteOn((long) Pm_MessageData1(midiInfo->buffer[0].message),(long) Pm_MessageData2(midiInfo->buffer[0].message)/127.);
			}
			else if((long)Pm_MessageStatus(midiInfo->buffer[0].message)==(0x80+midiInfo->channel)) //note off
			{
					midiInfo->Mannie->noteOff((long) Pm_MessageData1(midiInfo->buffer[0].message));

			}
			else if((long)Pm_MessageStatus(midiInfo->buffer[0].message)==(0xE0+midiInfo->channel))	//pitch bend
			{
				int value=(((int) Pm_MessageData2(midiInfo->buffer[0].message))<<7)+(int)Pm_MessageData1(midiInfo->buffer[0].message); //we combine MSB and LSB ->14bits value	
				midiInfo->Mannie->pitchBend(value);
			}
			
			else if((long)Pm_MessageStatus(midiInfo->buffer[0].message)==(0xB0+midiInfo->channel))	//control Change
			{
			//	Midi* tempMidi=(Midi*) midiInfo->Middle;
				((Midi*) midiInfo->Middle)->controlChange((long) Pm_MessageData1(midiInfo->buffer[0].message),(long) Pm_MessageData2(midiInfo->buffer[0].message));

			}
		
		}
	}
}

Midi::Midi(VoicesManager* Mmannie, Mixer* MMix)
{
	
	for(int i=0;i<512;i++)
	{
		map[i].active=false;
		map[i].someParam[0]=-1;
		map[i].someParam[1]=-1;
	}
	midiInfo.channel=0;
	midiInfo.Mannie=Mmannie;
	Mix=MMix;
	midiInfo.Middle=this;
	inDevice = Pm_GetDefaultInputDeviceID();
	devices=new midiDevice[Pm_CountDevices()];
    for (int i = 0; i < Pm_CountDevices(); i++) 
	{
        const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
        devices[i].name=info->name;
	devices[i].interface=info->interf;
	devices[i].id=i;
	if(info->input)
		devices[i].input=true;
	else
		devices[i].input=false;
	if(info->output)
		devices[i].output=true;
	else
		devices[i].output=false;
	/*cout<<endl<<devices[i].id<<" : "<<devices[i].interface<<"  "<<devices[i].name<<"	";
	if(devices[i].input)
	cout<<"midi In";
	if(devices[i].output)
	cout<<"midi Out";*/
 	}
	//cout<<endl;
	Pt_Start(MIDISPEED,&callback,&midiInfo);
    	Pm_OpenInput(&midiInfo.midas, inDevice,DRIVER_INFO, INPUT_BUFFER_SIZE,  TIME_PROC, TIME_INFO);//start midi stream
	Pm_SetFilter(midiInfo.midas, PM_FILT_ACTIVE | PM_FILT_CLOCK | PM_FILT_SYSEX);//set the filter to avoid useless messages
   	while (Pm_Poll(midiInfo.midas)) //we empty the buffer
	{
	        Pm_Read(midiInfo.midas, midiInfo.buffer, 1);
	}
//	armMapping(1,0,0,0);//test
	defaultCCMapping();
	
	
}

void Midi::changeDevice(int num)
{
	inDevice= num;
	Pt_Stop();
	Pm_Close(midiInfo.midas);
	Pt_Start(MIDISPEED,&callback,&midiInfo);
	Pm_OpenInput(&midiInfo.midas, inDevice,DRIVER_INFO, INPUT_BUFFER_SIZE,  TIME_PROC, TIME_INFO);//start midi stream
	Pm_SetFilter(midiInfo.midas, PM_FILT_ACTIVE | PM_FILT_CLOCK | PM_FILT_SYSEX);//set the filter to avoid useless messages
   	while (Pm_Poll(midiInfo.midas)) //we empty the buffer
	{
	        Pm_Read(midiInfo.midas, midiInfo.buffer, 1);
	}
	
	
	
}

void Midi::refreshDevicesList()
{
	bool prev=mapping;
	mapping=false;
	delete[] devices;
	Pt_Stop();
	Pm_Close(midiInfo.midas);
	Pm_Terminate();	//Yeap... We need to restart Pm if we want to refresh the list of devices...
	Pt_Start(MIDISPEED,&callback,&midiInfo);
	Pm_Initialize();
	devices=new midiDevice[Pm_CountDevices()];


	
	
    	for (int i = 0; i < Pm_CountDevices(); i++) 
	{
      	  const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
      	  devices[i].name=info->name;
		devices[i].interface=info->interf;
		devices[i].id=i;
		if(info->input)
			devices[i].input=true;
		else
			devices[i].input=false;
		if(info->output)
			devices[i].output=true;
		else
			devices[i].output=false;
		/*cout<<endl<<devices[i].id<<" : "<<devices[i].interface<<"  "<<devices[i].name<<"	";
		if(devices[i].input)
		cout<<"midi In";
		if(devices[i].output)
		cout<<"midi Out";*/
 	}
	if(inDevice<Pm_CountDevices())	//we just want to avoid the opening of an unplugged device
	{
		if(devices[inDevice].input==true)	//I assume nothing has changed
			;
		else 
			inDevice = Pm_GetDefaultInputDeviceID();	//brutal
	}
	else 
	{
		inDevice = Pm_GetDefaultInputDeviceID();	//soft
	}
	Pm_OpenInput(&midiInfo.midas, inDevice,DRIVER_INFO, INPUT_BUFFER_SIZE,  TIME_PROC, TIME_INFO);//start midi stream
	Pm_SetFilter(midiInfo.midas, PM_FILT_ACTIVE | PM_FILT_CLOCK | PM_FILT_SYSEX);
	  while (Pm_Poll(midiInfo.midas)) //we empty the buffer
	{
	        Pm_Read(midiInfo.midas, midiInfo.buffer, 1);
	}
	mapping=prev;
}


void Midi::armMapping(int module,int function,int param1,int param2)
{
	for(int i=0;i<512;i++)
	{
		if(map[i].active==false)
		{
			map[i].module=module;
			map[i].function=function;
			map[i].someParam[0]=param1;
			map[i].someParam[1]=param2;
			slotSelected=i;
			mapping=true;
			break;
		}
		
	}
}

void Midi::controlChange(int control,int value)	//for the moment, only CC, !NOT! NRPN
{
	//cout<<endl<<"CC : "<<control<<"			Value : "<<value;
	if(mapping==true && !(value>63 &&value<70))	//CC mapping, the user had selected a param, we're going to assign a CC for it //CC 64...69 are pedal switches ->Idon't want these controls to be map
	{
		map[slotSelected].CC=control;
		map[slotSelected].active=true;
		mapping=false;

	}
	else	
	{
		for(int i=0;i<512;i++)
		{
			if(map[i].active && map[i].CC==control)
				CCSwitch(i,value);
		}
	}
}

void Midi::defaultCCMapping()
{
	map[0].active=true;
	map[0].CC=64;	//sustain
	map[0].someParam[0]=-1;
	map[0].someParam[1]=-1;
	map[0].module=2;	//basic
	map[0].function=0;	//voicesManager->sustain
	map[1].active=true;
	map[1].CC=66;	//sostenuto
	map[1].someParam[0]=-1;
	map[1].someParam[1]=-1;
	map[1].module=2;	//basic
	map[1].function=1;	//voicesManager->sostenuto
}

void Midi::CCSwitch(int i, int value)
{
	if(map[i].module==0)	//mixer
	{
		if(map[i].function==0)//set group Param
		{
			Mix->setEffectParameter(map[i].someParam[0],map[i].someParam[1],value);
		
		}
		if(map[i].function==1)//voice volume someParam[0] is the group
					//need to think before doing this function 'cause of mutes and solos wich are managed by autogui...
		{
			for(int j=0;j<NBVOICES;j++)
			{
				if(map[i].someParam[0]==midiInfo.Mannie->returnVoiceGroup(j))
					Mix->setVoiceVolume(j,value/127.);
			}
		}
		if(map[i].function==2)//volume
		{
			Mix->setVolume(NBVOICES*value/127.);
		}
	}
	else if(map[i].module==1) //voices manager
	{
		if(map[i].function==0)//set group Param
		{
			midiInfo.Mannie->setGroupParameter(map[i].someParam[0],map[i].someParam[1],value);	
		}
	}
	else if(map[i].module==2)	//basic
	{
		if(map[i].function==0)//sustain
		{
			midiInfo.Mannie->sustainPedal(value);
		}
		else if(map[i].function==1)//sostenuto
		{
			midiInfo.Mannie->sostenutoPedal(value);
		}
	}	
}

void Midi::unMapSetP(int module,int param1)
{
	for(int i=0;i<512;i++)
	{
		if(map[i].active && map[i].module==module && map[i].someParam[0]==param1)
		{
			map[i].active=false;
		}
	}
}

void Midi::unMap(int module,int function,int param1,int param2)
{
	for(int i=0;i<512;i++)
	{
		if(map[i].active && map[i].module==module && map[i].function==function && map[i].someParam[0]==param1 && map[i].someParam[1]==param2)
		{
			map[i].active=false;
		}
	}
}

void Midi::unMapAll()
{
	for(int i=0;i<512;i++)
	{
		if(map[i].active && (map[i].module==0 || map[i].module==1))	//not really all, we do not unmap basic routing
		{
			map[i].active=false;
		}
	}	
}

Midi::~Midi()
{

	Pt_Stop();
	Pm_Close(midiInfo.midas);
	Pm_Terminate();
	delete[] devices;
}


