/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "utils.hpp"

using namespace std;

char getch() {	//AHAHAH
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}

void simple_keyboard(VoicesManager* Manie) //legacy, can be used in a terminal
{
			char temp=getch(); 
			if( temp=='w')
				Manie->noteOn((int)53,(float)1);
			else if( temp=='s')
				Manie->noteOn((int)54,(float)1);
			else if( temp=='x')
				Manie->noteOn((int)55,(float)1);
			else if( temp=='d')
				Manie->noteOn((int)56,(float)1);
			else if( temp=='c')
				Manie->noteOn((int)57,(float)1);
			else if( temp=='f')
				Manie->noteOn((int)58,(float)1);
			else if( temp=='v')
				Manie->noteOn((int)59,(float)1);
			else if( temp=='b')
				Manie->noteOn((int)60,(float)1);
			else if( temp=='h')
				Manie->noteOn((int)61,(float)1);
			else if( temp=='n')
				Manie->noteOn((int)62,(float)1);
			else if( temp=='j')
				Manie->noteOn((int)63,(float)1);
			else if( temp==',')
				Manie->noteOn((int)64,(float)1);
			/*else if(temp=='a'){
				Manie->modifyParameter(0,-1);}
			else if(temp=='z'){
				Manie->modifyParameter(0,1);}
			else if(temp=='e'){
				Manie->modifyParameter(1,-1);}
			else if(temp=='r'){
				Manie->modifyParameter(1,1);}
			else if(temp=='t'){
				Manie->modifyParameter(2,-1);}
			else if(temp=='y' ){
				Manie->modifyParameter(2,1);}
			else if(temp=='o' ){
				Manie->savePreset(0);}
			else if(temp=='p' ){
				int temp;
				cout<<endl<<"Preset Number :"<<endl;
				cin>>temp;
				Manie->loadPreset(temp);}*/
}

void simple_all_pass_filter(float* buffy,int size,float C)	//aka one sample delay
{
	float temp=0;
	float before=0;
	for(int i=0;i<size;i++)
	{
		before=buffy[i];
		buffy[i]=temp+C*before;
		temp=before-C*buffy[i];

	}
}

//now window functions

float * Wrectangular(int size)
{
	float* out=new float[size];
	for(int i=0;i<size;i++)
		out[i]=1;
	return out;
}

float* Wbartlett(int size)
{
	float* out=new float[size];
	float temp=size/2.;
	float tempc=2/(float)size;
	for(int i=0;i<size;i++)
	{
		float tempb= i-temp;
		if(tempb<0)
			tempb*=-1;
		out[i]=1-tempb*tempc;
	}
	return out;
}

float* Whanning(int size)
{
	float* out=new float[size];
	float temp=2*PI/(float)size;
	for(int i=0;i<size;i++)
		out[i]=0.5-0.5*cos(temp*i);
	return out;
		
}

float* Whamming(int size)
{
	float* out=new float[size];
	float temp=2*PI/(float)size;
	for(int i=0;i<size;i++)
		out[i]=0.54-0.46*cos(temp*i);
	return out;
		
}


float* Wblackman(int size)
{
	float* out=new float[size];
	float temp=2*PI/(float)size;
	for(int i=0;i<size;i++)
		out[i]=0.42-0.5*cos(temp*i)+0.08*cos(2*temp*i);
	return out;
		
}

float* Wrand(int size)
{
	float* out=new float[size];
	for(int i=0;i<size;i++)
		out[i]=rand()/(float)RAND_MAX;
	return out;
		
}




