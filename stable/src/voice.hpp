/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _VOICE_HPP_
#define _VOICE_HPP_
#include "main.hpp"

using namespace std;


/*
	virtual voice engine class
	the mother of all voices
	to write a new voices please overwrite all virtual functions=0
*/


class Voice
{
	protected :
	string name;	//name of the voice 
	int number;	//number of the voice, for the voices manager
	int bufferSize;
	std::mutex* mutexVar;	//do not delete this// You can use it, if you do some really nasty things, to block a thread (excite and callback are not executed in the same buffer but it's already managed elsewhere in the program, so,normally you don't have anything to do, unless really NASTY things)
	float* outputBuffer; //all the importants data for portaudio
	//please note that portaudio is STEREO,  so the buffer is organized like this : left, right,left+1,right+1,...,left+bufferSize/2, right+bufferSize/2
	string presetName;
	int numberParams;
	param* params;
	float speed=1;	//for cheap pitch bend
	public :
	
	Voice(string Vname,int Vnumber,int VbufferSize,std::mutex* VmutexVar, int nbparam);
	int returnNumberParams(){return numberParams;};
	string returnName(){return name;};
	param* returnParams(){return params;};
	//void init(string Vname,int Vnumber,int VbufferSize);
	virtual void debug()=0;	//can be usefull for debug, you can do whatever you want with this
	virtual int returnSize()=0;//not usefull now, does a voice counts like one voice or two or..
					//just a computationnal time estimation
	virtual void excite(float freq,float vel)=0;	//start note !
	virtual void callback(float* outputBuffer)=0;	//what the voice must do when portaudio wants some fresh data, return a buffer
	virtual void end()=0; //endnote
	virtual void modifyParameter(int param, int value)=0;//legacy
	virtual void setParameter(int param, int value)=0;//set the value of one parameter (the voice decides what it means, please use 8bits int (midi)
	void loadPreset(int preset); // load a specified preset
	void savePreset(int preset,string presetName);// save the current voice settings to the specified preset
	void createBank();
	string getPresetName(int preset);
	//for memory function, I think it must be managed by the voice virtual class directly!!!
	//we just need to save params, so the voice can do it!!!
	//just need to find the right implementation



	void pitchBend(float ispeed);	//change speed
	virtual ~Voice();
};


//just for convenience
class VoidVoice : public Voice
{
	private :
	float* voidBuffer;
	public :
	void debug()override{;};
	VoidVoice(int Knumber,int KbufferSize,std::mutex* KmutexVar);//
	int returnSize()override{return 1;};
	void excite(float freq,float vel)override{;};	//start note !
	void callback(float* outputBuffer)override{for(int i=0;i<bufferSize;i++)outputBuffer[i]=0;};	//what the voice must do when portaudio wants some fresh data, return a buffer
	void end()override{;}; //endnote
	void modifyParameter(int param, int value)override{;};// add the value to one parameter
	void setParameter(int param, int value)override{;};//set the value of one parameter

	~VoidVoice();
	
};

#endif
