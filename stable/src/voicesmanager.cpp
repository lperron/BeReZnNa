/*
BeReZnNa
Copyright (c) 2018 Lucas Perron

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "voicesmanager.hpp"

using namespace std;




VoicesManager::VoicesManager(int VnbVoices,Voice*** Vvoices, std::mutex* VmutexVar)
{
	mutexVar=VmutexVar;
	nbVoices=VnbVoices;
	numberGroup=1;
	assigned=new bool[VnbVoices];
	selected=new bool[VnbVoices];
	groupInfo=new GroupData[VnbVoices];
	group=new int[VnbVoices];
	time=new clock_t[VnbVoices];
	note=new int[VnbVoices];
	sostenustain=new int[VnbVoices];
	/*ifstream scale("./Scales/Well Tuned.noodle", ios::in);
	if(scale)
	{
		for(int i=0;i<128;i++)
			scale>>midiToHz[i];
		scale.close();
	}
	else*/
	tuning=new float[12];
	loadScl("./Scales/welltuned12.scl");
	ref=51;
	freq=155.5634918610;
	updatePitch();
	
	voices=*Vvoices;

	//cout<<endl<<nbVoices<<endl;

	//simple initialization, at the end we will have a group of 8 karplus-strong
	for(int i=0;i<nbVoices;i++)
	{
		assigned[i]=false;
		selected[i]=true;
		time[i]=0;
		note[i]=0;
		group[i]=0;
		groupInfo[i].groupVoices=0;
		groupInfo[i].active=false;
		groupInfo[i].min=0;
		groupInfo[i].max=127;	
		sostenustain[i]=0;
	}
	groupInfo[0].groupVoices=8;
	groupInfo[0].active=true;
	nbFreeVoices=0;
	for(int i=0;i<8;i++)
		voices[i]=initEngine(i,mutexVar,0);

	for(int i=8;i<nbVoices;i++)
		voices[i]=new VoidVoice(i,BUFF,mutexVar);
	for(int i=8;i<nbVoices;i++)
	{
		group[i]=-1;
		nbFreeVoices++;
	}
	//for(int i=0;i<200;i++)
	//cout<<endl<<i<<"  : "<<getPresetName(i,0)<<endl;
	loadPreset(0,0);
	//addGroup(1,8,0,0,127);	//all these functions work well
	//deleteGroup(0);
	//modifyGroup(0,-7,0);
	//modifyGroup(0,7,0);
}

int VoicesManager::returnGroupNumberParam(int Vgroup)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==Vgroup)
			return voices[i]->returnNumberParams();
	}
	return 0;
}
string VoicesManager::returnGroupName(int Vgroup)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==Vgroup)
			return voices[i]->returnName();
	}
	return "None";
}
param* VoicesManager::returnGroupParams(int Vgroup)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==Vgroup)
			return voices[i]->returnParams();
	}
	return NULL;
}


void VoicesManager::noteOn(int Vnote,float Vvelocity)	//can be easily optimized, but it's a non-critical function, SO I will do this later !!!
{

	/*so the priority is : -first voices wich are not playing a note (assigned==false)
					-if multiple, the oldest unassigned
				-else the oldest assigned voice
	
		
		*/
	int tempb=0;	
	clock_t temp;
	bool tempc;
	//first we need to know which voice we're gonna use
	//I don't know what this thing does but it seems to be ok
	for(int j=0;j<nbVoices;j++)
	{
		bool sendNote=false;
		if(Vnote>=groupInfo[j].min &&Vnote<=groupInfo[j].max and groupInfo[j].active==true)
		{
			sendNote=true;
			bool first=true;
			for(int i=0;i<nbVoices;i++)
			{
				if(group[i]==j)
				{
					
					if(first==true)
					{
						temp=time[i];	//my favorite time
						tempb=i;//my favorite index
						tempc=assigned[i];//my favorite state of mind
						first=false;
						//Yep, even me don't understand what I mean;
					}
					else if(tempc==false)
					{
						if(time[i]<temp and assigned[i]==false)
						{
							temp=time[i];
							tempb=i;
						}
					}
					else if(tempc==true)
					{	
						if(assigned[i]==false)
						{
							temp=time[i];
							tempb=i;
							tempc=false;
						}
						if(assigned[i]==true)
						{
							if(time[i]<temp)
							{
								temp=time[i];
								tempb=i;
							}
						}
					}
				}
			}	
			if(sendNote==true)
			{
				note[tempb]=Vnote;
				mutexVar->lock();
				voices[tempb]->excite(midiToHz[Vnote],Vvelocity); //here
				mutexVar->unlock();
				time[tempb]=clock();
				assigned[tempb]=true;
				if(sustain)
					sostenustain[tempb]=0;
			}
		}
	}

	
}

void VoicesManager::noteOff(int Vnote)	//check each voice if it s playing the Vnote, and if true send a end message to this voice, unassign and refresh time
{

		for(int i=0;i<nbVoices;i++)
		{
			if(Vnote==note[i])
			{
				if(!sustain && (sostenustain[i]<1))
				{
					voices[i]->end();
					note[i]=-1;
					assigned[i]=false;   
					time[i]=clock();
				}
				else if(sustain || (sostenuto && sostenustain[i]==2))
					sostenustain[i]=1;

			}
		}
}

void VoicesManager::sustainPedal(int value)
{
	if(value<64)//off
	{
		for(int i=0;i<NBVOICES;i++)
		{
			if(sostenustain[i]==1)
			{
				voices[i]->end();
				note[i]=-1;
				assigned[i]=false;   
				time[i]=clock();
				sostenustain[i]=0;	
			}
		}
		sustain=false;
	}
	else	//on
	{
		sustain=true;
	}
}	

void VoicesManager::sostenutoPedal(int value)
{
	if(value<64)//off
	{
		for(int i=0;i<NBVOICES;i++)
		{
			if(sostenustain[i]==1)
			{
				voices[i]->end();
				note[i]=-1;
				assigned[i]=false;   
				time[i]=clock();
				sostenustain[i]=0;	
			}
		}
		sostenuto=false;
	}
	else	//on
	{
		sostenuto=true;
		for(int i=0;i<NBVOICES;i++)
		{
				if(assigned[i]==true)
					sostenustain[i]=2;
		}
	}
}	

void VoicesManager::modifyParameter(int param,int value)//legacy don't care
{
	for(int i=0;i<nbVoices;i++)
	{
		if(selected[i]==true)
		{
			voices[i]->modifyParameter(param,value);
		}
	}
}

void VoicesManager::setParameter(int param,int value)//legacy don't care (now BeReZnNa uses group, not selection)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(selected[i]==true)
		{
			voices[i]->setParameter(param,value);
		}
	}
}

void VoicesManager::setGroupParameter(int gro,int para,int value)//check each voice to see if it is in the right group then send a message
{
	for(int i=0;i<nbVoices;i++)
	{
		if(gro==group[i])
			voices[i]->setParameter(para,value);
	}
}

void VoicesManager::pitchBend(int value)
{
	float speed=0;
	float temp=(value-8192)/8192.;	//between -1 and 1
	temp*=range;	//between -range & range
	
	if(temp>0)
		speed=1+temp;
	else
	{
		speed=1/(1-temp);
	}
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]!=-1)
			voices[i]->pitchBend(speed);
	}
}

void VoicesManager::loadPreset(int preset,int gro)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==gro)
		{
			voices[i]->loadPreset(preset);
		}
	}	
}

string VoicesManager::getPresetName(int preset,int gro)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==gro)
		{
			return voices[i]->getPresetName(preset);
		}
	}	
	return "None";
}

void VoicesManager::savePreset(int preset, string presetName,int gro)	
{
	bool ok=false;
	int i=0;
	do{
		if(group[i]==gro)
		{
			voices[i]->savePreset(preset,presetName);
			ok=true;	
		}
		i++;
	}while(ok==false and i<nbVoices);
}

void VoicesManager::addGroup(int gro, int voice,int eng,int imin,int imax)// create a new group with the specify params
{
	int temp=0;
	bool ok=false;
	groupInfo[gro].groupVoices=0;
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==-1 && temp<voice)
		{
			
			ok=true;
			group[i]=gro;
			mutexVar->lock();
			delete voices[i];
			voices[i]=initEngine(i,mutexVar,eng);
			mutexVar->unlock();
			nbFreeVoices--;
			groupInfo[gro].groupVoices++;
			temp++;
		}
	}
	if(ok==true)
	{
		numberGroup++;
		groupInfo[gro].active=true;
		groupInfo[gro].min=imin;
		groupInfo[gro].max=imax;
		loadPreset(0,gro);
	}
}

void VoicesManager::deleteGroup(int gro)
{
	for(int i=0;i<nbVoices;i++)
	{
		if(group[i]==gro)
		{
			assigned[i]=false;
			mutexVar->lock();
			delete voices[i];
			voices[i]=new VoidVoice(i,BUFF,mutexVar);
			mutexVar->unlock();
			group[i]=-1;
			groupInfo[gro].groupVoices--;
			nbFreeVoices++;
		}
	}	
	groupInfo[gro].active=false;
	numberGroup--;
}

void VoicesManager::modifyGroup(int gro,int voice,int eng)	//not used for the moment, but it's working
{
	int temp=0;
	if(voice<0)	//we want less voice in this group
	{
		for(int i=0;i<nbVoices;i++)
		{
			if(group[i]==gro && temp>voice)
			{
				assigned[i]=false;
				mutexVar->lock();
				delete voices[i];
				voices[i]=new VoidVoice(i,BUFF,mutexVar);
				mutexVar->unlock();
				group[i]=-1;
				groupInfo[gro].groupVoices--;
				nbFreeVoices++;
				temp--;
			}
		}

	}
	if(voice>0)
	{
		for(int i=0;i<nbVoices;i++)
		{
			if(group[i]==-1 && temp<voice)
			{
				mutexVar->lock();
				delete voices[i];
				voices[i]=initEngine(i,mutexVar,eng);
				mutexVar->unlock();
				group[i]=gro;
				groupInfo[gro].groupVoices++;
				nbFreeVoices--;
				temp++;
			}
		}
	}

}

void VoicesManager::changeScale(string file)
{
		ifstream scale(file.c_str(), ios::in);
	if(scale)
	{
		for(int i=0;i<128;i++)
			scale>>midiToHz[i];
		scale.close();
	}
	else
	{
		cout<<endl<<"No scale : loading default"<<endl;
		int maBenz = 440; //  440 hz...
		for (int x = 0; x < 128; ++x)	
		{
			midiToHz[x] = (maBenz / 32.) * pow(2,((x-9)/12.));
		}
	}
	noodle=true;
}

void VoicesManager::loadScl(string file)
{
	ifstream scale(file.c_str(),ios::in);	//we open the scl file
	int scaleSize=-1;

	string line;

		first=0;
		last=127;
		ref=69;
		freq= 440;
		notesPerOctave=-1;
	if(scale)
	{
		bool desc=false;
		scaleSize=-1;
		int indexTab=0;
		
		while(getline(scale,line))//we read it line per line
		{
			if(line[0]!='!')	//not a comment
			{
				if(desc==false)	//first thing is the desc
				{
					desc=true;	//maybe we can do something something with desc but for the moment I don't care at all !
					continue;
				}
				else if(scaleSize==-1)	//then the size
				{
					istringstream temp(line);
					temp>>scaleSize;
					//if(scaleSize>128)
					//	cout<<endl<<"error, not a valid scale"<<endl;
					//else
					{
						delete[] tuning;
						tuning=new float[scaleSize];
						if(notesPerOctave==-1)
							notesPerOctave=scaleSize;
					}
				}
				else if(scaleSize!=-1 && indexTab<scaleSize)	//now the tuning
				{
					if(line.find('.')!=string::npos) //it's cent
					{
						istringstream temp(line);
						string stemp="a";
						char ctemp;
						bool end=false;
						while(temp.get(ctemp) and end==false)
						{
							if(ctemp!=' ')
							{
								if(stemp=="a")
									stemp=ctemp;
								else
									stemp=stemp+ctemp;
							}
							else if(stemp!="a") //we already have what we want
								end=true;
						}
						istringstream finalTuning(stemp);
						finalTuning>>tuning[indexTab];
						tuning[indexTab]=pow(2.,tuning[indexTab]/1200.);
						indexTab++;
					}
					else  //it's a ratio
					{

						if(line.find('/')!=string::npos) //it's really a ratio
						{
							int a;
							float b;

							/*a=line[line.find('/')-1]-'0';
							b=line[line.find('/')+1]-'0';*/ //some guys don't respect the specifications.....
							istringstream temp(line);
							string stemp="a";
							char ctemp;
							bool end=false;
							unsigned int index=0;
							while(temp.get(ctemp) and end==false)
							{
								if(ctemp!=' ' and ctemp!='/')
								{
									if(stemp=="a")
										stemp=ctemp;
									else
										stemp=stemp+ctemp;
									if(index==line.size()-1)
									{
										istringstream finalb(stemp);
										finalb>>b;

										end=true;
									}
								}
								else if(ctemp=='/')
								{
									istringstream finala(stemp);
									finala>>a;
									stemp="a";

								}
								else if(stemp!="a") //we already have what we want
								{
									istringstream finalb(stemp);
									finalb>>b;

									end=true;
								}
								index++;
							}
							tuning[indexTab]=(float)a/b;
							indexTab++;	
						}
						else //I assume it's a single number
						{
							istringstream temp(line);
							temp>>tuning[indexTab];
							indexTab++;
						}
					}
				}
			}
					
				
				
	
			
		}
		
		//cout<<endl<<"Size : "<<scaleSize<<endl;
		//for(int i=0;i<scaleSize;i++)
		//	cout<<endl<<i<<" : "<<tuning[i];
	//	cout<<endl<<endl;
		scale.close();

		//we have all the tuning data

		//now we can compute the freq of each note
		//default
		if(last!=127 && first!=0)
		{
			int maBenz = freq; 
			for (int x = 0; x < 128; ++x)	
			{
				midiToHz[x] = (maBenz / 32.) * pow(2,((x-9)/12.));
			}
		}

		for(int i=first;i<=last;i++)
		{
			int note=i-ref;
			int deg=note%(notesPerOctave);	
			if(deg<0)
				deg+=notesPerOctave;
			int oct=floor((note)/(float)(notesPerOctave));	
			float f=freq*pow(tuning[notesPerOctave-1],oct);
			if(deg>0)
				f=f*tuning[deg-1];
			midiToHz[i]=f;
			//cout<<endl<<i<<" : "<<f<<endl;

		}
		noodle=false;	
	}
	else
	{
		cout<<endl<<"No scale : loading default"<<endl;
		int maBenz = 440; //  440 hz...
		for (int x = 0; x < 128; ++x)	
		{
			midiToHz[x] = (maBenz / 32.) * pow(2,((x-9)/12.));
		}
		noodle=true;
	}
}

void VoicesManager::setRef(int refe)
{
	ref=refe;

}

void VoicesManager::setFreq(float frequ)
{
	freq=frequ;

}

void VoicesManager::updatePitch()
{
	if(noodle==false)
	{
		for(int i=first;i<=last;i++)
		{
			//midiToHz[i]=freq*tuning[i];
			int note=i-ref;	//5-69=-64
			int deg=note%(notesPerOctave);	// 0
			if(deg<0)
				deg+=notesPerOctave;
			int oct=floor((note)/(float)(notesPerOctave));	//-1
			//float f=freq*pow(tuning[notesPerOctave-1],oct*notesPerOctave/notesPerOctave);
			float f=freq*pow(tuning[notesPerOctave-1],oct);
			if(deg>0)
				f=f*tuning[deg-1];
			f=min((double)max(f,(float)20),SAMPLE_RATE/2.);	//just to have a frequency between 20Hz and SAMPLE_RATE/2

			midiToHz[i]=f;
			

		}
	}
}
VoicesManager::~VoicesManager()
{	
	//mutexVar will not be deleted here
	delete[] time;
	delete[] assigned;
	delete[] note;
	delete[] selected;
	delete[]group;
	delete[] groupInfo;
	delete[] tuning;
	delete[] sostenustain;
}
